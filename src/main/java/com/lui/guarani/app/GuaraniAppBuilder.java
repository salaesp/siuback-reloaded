package com.lui.guarani.app;

import com.google.common.collect.Lists;
import com.lui.guarani.model.*;
import com.lui.guarani.model.id.SubjectID;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by lui on 19/05/15.
 */
public class GuaraniAppBuilder {

	public static AppBuilderResponse buildData() {

		Date currentDate = Calendar.getInstance().getTime();

		InscriptionVariant examVariantInf = new InscriptionVariant("134", currentDate);
		InscriptionVariant subjectVariantInf = new InscriptionVariant("166", currentDate);


		AcademicUnit informaticaAU = GuaraniAppHelper.getAcademicUnitFor("Informatica");

		Subject program = GuaraniAppHelper.getSubjectFor("Programacion");
		program.getExamVariants().add(examVariantInf);
		program.getSubjectInscriptionVariants().add(subjectVariantInf);
		Subject math = GuaraniAppHelper.getSubjectFor("Matematicas");
		math.getExamVariants().add(examVariantInf);
		math.getSubjectInscriptionVariants().add(subjectVariantInf);
		Subject arq = GuaraniAppHelper.getSubjectFor("Arquitectura");
		arq.getExamVariants().add(examVariantInf);
		arq.getSubjectInscriptionVariants().add(subjectVariantInf);
		Subject program2 = GuaraniAppHelper.getSubjectFor("Programacion 2");
		program2.getExamVariants().add(examVariantInf);
		program2.getSubjectInscriptionVariants().add(subjectVariantInf);
		Subject program3 = GuaraniAppHelper.getSubjectFor("Programacion 3");
		program3.getExamVariants().add(examVariantInf);
		program3.getSubjectInscriptionVariants().add(subjectVariantInf);
		Subject ing = GuaraniAppHelper.getSubjectFor("Ingenieria");
		ing.getExamVariants().add(examVariantInf);
		ing.getSubjectInscriptionVariants().add(subjectVariantInf);

		List<Subject> sisSubjects = Lists.newArrayList(program, math, arq, program2, program3, ing);

		Career licenciaturaEnSistemas = GuaraniAppHelper.getCareerFor("Licenciatura en Sistemas");

		CareerPlan sisPlan2013 = GuaraniAppHelper.getCareerPlanFor("Plan 2013");
		sisPlan2013.addSubjects(sisSubjects);
		licenciaturaEnSistemas.addCareerPlans(Lists.newArrayList(sisPlan2013));

		informaticaAU.addCareer(licenciaturaEnSistemas);
		informaticaAU.update();

		program2.addExamDependency(program);
		program3.addExamDependency(program2);
		ing.addExamDependency(math);
		ing.addExamDependency(arq);

//		psicologia

		InscriptionVariant examVariantPsi = new InscriptionVariant("154", currentDate);
		InscriptionVariant subjectVariantPsi = new InscriptionVariant("108", currentDate);


		AcademicUnit psicologiaAU = GuaraniAppHelper.getAcademicUnitFor("Psicologia");

		Subject psi = GuaraniAppHelper.getSubjectFor("Psicologia");
		psi.getExamVariants().add(examVariantPsi);
		psi.getSubjectInscriptionVariants().add(subjectVariantPsi);
		Subject psi2 = GuaraniAppHelper.getSubjectFor("Psicologia 2");
		psi2.getExamVariants().add(examVariantPsi);
		psi2.getSubjectInscriptionVariants().add(subjectVariantPsi);
		Subject fil = GuaraniAppHelper.getSubjectFor("Filosofia");
		fil.getExamVariants().add(examVariantPsi);
		fil.getSubjectInscriptionVariants().add(subjectVariantPsi);
		List<Subject> psiSubjects = Lists.newArrayList(psi, psi2, fil);

		Career psicologiaCareer = GuaraniAppHelper.getCareerFor("Psicologia");
		CareerPlan psiPlan2012 = GuaraniAppHelper.getCareerPlanFor("Plan 2012");
		psiPlan2012.addSubjects(psiSubjects);
		psicologiaCareer.addCareerPlan(psiPlan2012);

		psicologiaAU.addCareer(psicologiaCareer);
		psicologiaAU.update();
		psi2.addExamDependency(psi);

		ArrayList<AcademicUnit> academicUnits = Lists.newArrayList(informaticaAU, psicologiaAU);

		//Users

		Student luisStudent = new Student("2139", "Luis Manuel Sala Espiell");
		Student pepitoStudent = new Student("5656", "Pepito Perez");
		Student pauStudent = new Student("1234", "Paula Kolbl");


		String careerInscriptionID = GuaraniAppHelper.getId(CareerInscription.class, currentDate, luisStudent);
		CareerInscription sisCareerInscription = new CareerInscription(careerInscriptionID, careerInscriptionID, currentDate, sisPlan2013.getAcademicUnit(), sisPlan2013.getCareer(), sisPlan2013.getId());
		luisStudent.setCareerInscriptions(Lists.newArrayList(sisCareerInscription));

		String careerInscriptionIDPepito = GuaraniAppHelper.getId(CareerInscription.class, currentDate, pepitoStudent);
		CareerInscription sisCareerInscriptionPepito = new CareerInscription(careerInscriptionIDPepito, careerInscriptionIDPepito, currentDate, sisPlan2013
				.getAcademicUnit(), sisPlan2013.getCareer(), sisPlan2013.getId());
		pepitoStudent.setCareerInscriptions(Lists.newArrayList(sisCareerInscriptionPepito));

		SubjectID programSubjectId = new SubjectID(sisPlan2013.getAcademicUnit(), sisPlan2013.getCareer(), sisPlan2013.getId(), program.getId());
		SubjectID mathSubjectId = new SubjectID(sisPlan2013.getAcademicUnit(), sisPlan2013.getCareer(), sisPlan2013.getId(), math.getId());
		SubjectID program2SubjectId = new SubjectID(sisPlan2013.getAcademicUnit(), sisPlan2013.getCareer(), sisPlan2013.getId(), program2.getId());


		Inscription examInscription = new Inscription(currentDate, GuaraniAppHelper.getId(Inscription.class, currentDate, luisStudent),
				programSubjectId, examVariantInf);
		luisStudent.setExamInscriptions(Lists.newArrayList(examInscription));

		String programRegularityID = GuaraniAppHelper.getIdForDateIdObject(currentDate, program, luisStudent, SubjectRegularity.class);
		SubjectRegularity programRegularity = new SubjectRegularity(programRegularityID, programRegularityID, currentDate, programSubjectId);
		String mathRegularityID = GuaraniAppHelper.getIdForDateIdObject(currentDate, math, luisStudent, SubjectRegularity.class);
		SubjectRegularity mathRegularity = new SubjectRegularity(mathRegularityID, mathRegularityID, currentDate, mathSubjectId);
		luisStudent.setSubjectRegularities(Lists.newArrayList(programRegularity, mathRegularity));

		String programExamId = GuaraniAppHelper.getIdForDateIdObject(currentDate, program, luisStudent, Exam.class);
		Exam programExam = new Exam(currentDate, programExamId, programSubjectId, programExamId, 8.5);
		luisStudent.setExams(Lists.<Exam>newArrayList(programExam));

		Inscription mathInscription = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, math, luisStudent,
				Inscription.class), mathSubjectId, subjectVariantInf);
		Inscription programInscription = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, program, luisStudent, Inscription.class), programSubjectId, subjectVariantInf);
		Inscription program2Inscription = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, program2, luisStudent, Inscription.class), program2SubjectId, subjectVariantInf);

		luisStudent.setSubjectInscriptions(Lists.newArrayList(mathInscription, program2Inscription, programInscription));


		String programRegularityIDPepito = GuaraniAppHelper.getIdForDateIdObject(currentDate, program, pepitoStudent, SubjectRegularity.class);
		SubjectRegularity programRegularityPepito = new SubjectRegularity(programRegularityIDPepito, programRegularityIDPepito, currentDate,
				programSubjectId);
		pepitoStudent.setSubjectRegularities(Lists.newArrayList(programRegularityPepito));

		Inscription programInscriptionPepito = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, program, pepitoStudent,
				Inscription.class), programSubjectId, subjectVariantInf);
		Inscription program2InscriptionPepito = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, program2,
				pepitoStudent,
				Inscription.class), program2SubjectId, subjectVariantInf);
		pepitoStudent.setSubjectInscriptions(Lists.newArrayList(programInscriptionPepito, program2InscriptionPepito));


		User luis = new User("1", "luissala", "1234", luisStudent);
		User pkolbl = new User("2", "pkolbl", "1234", pauStudent);
		User pepito = new User("3", "pepito", "1234", pepitoStudent);

		AppBuilderResponse appBuilderResponse = new AppBuilderResponse();
		appBuilderResponse.setAcademicUnitList(academicUnits);
		appBuilderResponse.setUserList(Lists.newArrayList(luis, pkolbl, pepito));
		return appBuilderResponse;
	}
}
