package com.lui.guarani.app;

import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.lui.guarani.model.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by lui on 22/05/15.
 */
public class GuaraniAppHelper {

	public static String getId(Class clazz, Date date, Student student) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(clazz);
		buffer.append(date);
		buffer.append(student);
		buffer.append(Calendar.getInstance().getTime());
		return String.valueOf(buffer.hashCode());
	}
	public static String getId(Class clazz, Date date, Student student, Object o) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(clazz);
		buffer.append(date);
		buffer.append(student);
		buffer.append(o);
		buffer.append(Calendar.getInstance().getTime());
		return String.valueOf(buffer.hashCode());
	}

	public static String getIdForDateIdObject(Date date, Subject subject, Student student, Class clazz) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(date);
		buffer.append(subject);
		buffer.append(student);
		buffer.append(clazz);
		return String.valueOf(buffer.hashCode());
	}


	public static Career getCareerFor(String name) {
		return new Career(getId(name), name);
	}

	public static AcademicUnit getAcademicUnitFor(String name) {
		return new AcademicUnit(getId(name), name);
	}

	public static CareerPlan getCareerPlanFor(String name) {
		return new CareerPlan(getId(name), name);
	}

	public static Subject getSubjectFor(String name) {
		return new Subject(getId(name), name);
	}

	public static String getId(String name) {
		return String.valueOf(name.hashCode());
	}


	public static <T extends AbstractIdentifiableObject> Map<String, T> getAsMap(List<T> basicIdentifiableList) {
		return Maps.uniqueIndex(basicIdentifiableList, new Function<T, String>() {
			@Override
			public String apply(T t) {
				return t.getId();
			}
		});
	}

}
