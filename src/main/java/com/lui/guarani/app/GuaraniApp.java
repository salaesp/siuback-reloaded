package com.lui.guarani.app;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.lui.guarani.model.*;
import com.lui.guarani.model.id.SubjectID;
import com.lui.service.DebugService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by lui on 19/05/15.
 */
@Component
public class GuaraniApp {

    final static Logger LOG = Logger.getLogger(GuaraniApp.class);

    private Map<String, AcademicUnit> academicUnitMap;
    private Map<String, User> userMap;
    @Autowired
    private DebugService debugService;

    @PostConstruct
    public void init() throws IOException {
        AppBuilderResponse appBuilderResponse = debugService.buildGuaraniData();
        userMap = GuaraniAppHelper.getAsMap(appBuilderResponse.getUserList());
        academicUnitMap = GuaraniAppHelper.getAsMap(appBuilderResponse.getAcademicUnitList());
    }

    public Map<String, User> getUserMap() {
        return userMap;
    }

    public User login(final String username, final String password) {
        Optional<User> userOptional = Iterables.tryFind(userMap.values(), new Predicate<User>() {
            @Override
            public boolean apply(User user) {
                return user.getUsername().equals(username) && user.getPassword().equals(password);
            }
        });
        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        return null;
    }


    public List<AcademicUnit> getAcademicUnits() {
        return Lists.newArrayList(academicUnitMap.values());
    }

    public AcademicUnit getAcademicUnit(String academicUnitId) {
        return academicUnitMap.get(academicUnitId);
    }

    public List<Career> getCareers(String academicUnitId) {
        if (!academicUnitMap.containsKey(academicUnitId)) {
            return null;
        }
        return Lists.newArrayList(this.getAcademicUnit(academicUnitId).getCareers().values());
    }

    public Career getCareer(String academicUnitId, String careerId) {
        if (!academicUnitMap.containsKey(academicUnitId)) {
            return null;
        }
        return this.getAcademicUnit(academicUnitId).getCareers().get(careerId);
    }

    public List<CareerPlan> getCareerPlans(String academicUnitId, String careerId) {
        if (this.getAcademicUnit(academicUnitId) != null && this.getAcademicUnit(academicUnitId).getCareers().containsKey(careerId)) {
            return Lists.newArrayList(this.getCareer(academicUnitId, careerId).getCareerPlanMap().values());
        }
        return null;
    }

    public CareerPlan getCareerPlan(String academicUnitId, String careerId, String careerPlanId) {
        if (this.getCareer(academicUnitId, careerId) != null && this.getCareer(academicUnitId, careerId).getCareerPlanMap().containsKey(careerPlanId)) {
            return this.getCareer(academicUnitId, careerId).getCareerPlanMap().get(careerPlanId);
        }
        return null;
    }

    public List<Subject> getSubjects(String academicUnitId, String careerId, String careerPlanId) {
        CareerPlan plan = this.getCareerPlan(academicUnitId, careerId, careerPlanId);
        if (plan != null && !plan.getSubjectMap().isEmpty()) {
            return Lists.newArrayList(plan.getSubjectMap().values());
        }
        return null;
    }

    public Subject getSubject(String academicUnitId, String careerId, String careerPlanId, String subjectId) {
        CareerPlan careerPlan = this.getCareerPlan(academicUnitId, careerId, careerPlanId);
        if (careerPlan != null && careerPlan.getSubjectMap().containsKey(subjectId)) {
            return careerPlan.getSubjectMap().get(subjectId);
        }
        return null;
    }

    public List<CareerInscription> getUserCareerInscriptions(String id) {
        return userMap.get(id).getStudent().getCareerInscriptions();
    }

    public List<Exam> getUserExams(String userId) {
        return userMap.get(userId).getStudent().getExams();
    }

    public List<SubjectRegularity> getUserSubjectRegularities(String userId) {
        List<SubjectRegularity> subjectRegularities = userMap.get(userId).getStudent().getSubjectRegularities();
        return subjectRegularities;
    }

    public List<Inscription> getUserSubjectInscriptions(String userId) {
        return userMap.get(userId).getStudent().getSubjectInscriptions();
    }

    public List<Inscription> getUserExamInscriptions(String userId) {
        return userMap.get(userId).getStudent().getExamInscriptions();
    }

    public Inscription addSubjectInscription(Subject subject, String userId, InscriptionVariant variant) {
        Date now = Calendar.getInstance().getTime();
        Student student = userMap.get(userId).getStudent();
        Inscription inscription = new Inscription(now, GuaraniAppHelper.getIdForDateIdObject(now, subject, student,
                Inscription.class), new SubjectID(subject.getCareerPlanID(), subject.getId()), variant);
        student.addSubjectInscription(inscription);
        LOG.debug(String.format("Inscription %s added to subject inscriptions. Size %s", inscription, student.getSubjectInscriptions().size()));
        return inscription;
    }

    public Inscription addExamInscription(Subject subject, String userId, InscriptionVariant variant) {
        Date now = Calendar.getInstance().getTime();
        Student student = userMap.get(userId).getStudent();
        Inscription inscription = new Inscription(now, GuaraniAppHelper.getIdForDateIdObject(now, subject, student,
                Inscription.class), new SubjectID(subject.getCareerPlanID(), subject.getId()), variant);
        student.addExamInscription(inscription);
        return inscription;
    }

    public void deleteExamInscription(String userId, Inscription inscription) {
        Student student = userMap.get(userId).getStudent();
        student.getExamInscriptions().remove(inscription);
    }

    public void deleteSubjectInscription(String userId, Inscription inscription) {
        Student student = userMap.get(userId).getStudent();
        student.getSubjectInscriptions().remove(inscription);
    }

    public List<Student> getStudentsFor(final SubjectID subject) {
        return FluentIterable.from(this.userMap.values()).filter(new Predicate<User>() {
            @Override
            public boolean apply(User user) {
                if (user.getStudent().getSubjectInscriptions() == null) {
                    return false;
                }
                return Iterables.tryFind(user.getStudent().getSubjectInscriptions(), new Predicate<Inscription>() {
                    @Override
                    public boolean apply(Inscription inscription) {
                        return inscription.getSubject().equals(subject);
                    }
                }).isPresent();
            }
        }).transform(new Function<User, Student>() {
            @Override
            public Student apply(User user) {
                return user.getStudent();
            }
        }).toList();
    }

    public User getUserFor(final Student student) {
        return Iterables.find(userMap.values(), new Predicate<User>() {
            @Override
            public boolean apply(User user) {
                return user.getStudent().getId().equals(student.getId());
            }
        });
    }

    public Student getStudentWithUserId(String userId) {
        return this.userMap.get(userId).getStudent();
    }

}
