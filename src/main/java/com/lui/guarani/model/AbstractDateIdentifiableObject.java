package com.lui.guarani.model;

import java.util.Date;

/**
 * Created by lui on 22/05/15.
 */
public abstract class AbstractDateIdentifiableObject extends AbstractIdentifiableObject {

	private Date date;

	public AbstractDateIdentifiableObject(String id, String name, Date date) {
		super(id, name);
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
