package com.lui.guarani.model;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Map;

/**
 * Created by lui on 19/05/15.
 */
// Unidad academica representation
public class AcademicUnit extends AbstractIdentifiableObject implements IUpdateableModel {
	private Map<String, Career> careers;

	public AcademicUnit(String id, String name) {
		super(id, name);
		careers = Maps.newHashMap();
	}

	public void addCareer(Career career) {
		career.setAcademicUnit(this.getId());
		careers.put(career.getId(), career);
	}

	public Map<String, Career> getCareers() {
		return careers;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public void update() {
		for (Career career : careers.values()) {
			career.setAcademicUnit(this.getId());
			career.update();
		}
	}
}
