package com.lui.guarani.model;

import com.lui.guarani.model.id.SubjectID;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Date;

/**
 * Created by lui on 19/05/15.
 */
public class Inscription extends AbstractSubjectAwareObject {

	private InscriptionVariant variant;

	public Inscription(Date inscriptionDate, String id, SubjectID subjectCode, InscriptionVariant variant) {
		super(inscriptionDate, id, subjectCode, "inscription" + subjectCode.hashCode());
		this.variant = variant;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public InscriptionVariant getVariant() {
		return variant;
	}

	public void setVariant(InscriptionVariant variant) {
		this.variant = variant;
	}
}
