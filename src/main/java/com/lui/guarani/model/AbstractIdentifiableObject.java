package com.lui.guarani.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by lui on 19/05/15.
 */
public abstract class AbstractIdentifiableObject {

	private String id;
	private String name;

	public AbstractIdentifiableObject(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
