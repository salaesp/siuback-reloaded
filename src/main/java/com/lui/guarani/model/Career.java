package com.lui.guarani.model;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by lui on 19/05/15.
 */
public class Career extends AbstractIdentifiableObject implements IUpdateableModel {

	private String academicUnit;

	private Map<String, CareerPlan> careerPlanMap;

	public Career(String id, String name) {
		super(id, name);
		this.careerPlanMap = Maps.newHashMap();
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public void update() {
		for (CareerPlan careerPlan : careerPlanMap.values()) {
			careerPlan.setAcademicUnit(this.getAcademicUnit());
			careerPlan.setCareer(this.getId());
			careerPlan.update();
		}
	}

	public Map<String, CareerPlan> getCareerPlanMap() {
		return careerPlanMap;
	}

	public void addCareerPlan(CareerPlan plan) {
		plan.setAcademicUnit(this.getAcademicUnit());
		plan.setCareer(this.getId());
		this.careerPlanMap.put(plan.getId(), plan);
	}

	public void addCareerPlans(List<CareerPlan> plans) {
		for (CareerPlan plan : plans) {
			this.addCareerPlan(plan);
		}
	}

	public String getAcademicUnit() {
		return academicUnit;
	}

	public void setAcademicUnit(String academicUnit) {
		this.academicUnit = academicUnit;
	}
}
