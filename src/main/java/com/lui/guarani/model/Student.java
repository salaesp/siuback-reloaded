package com.lui.guarani.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by lui on 19/05/15.
 */
public class Student extends AbstractIdentifiableObject {

	final static Logger LOG = Logger.getLogger(Student.class);

	private List<Inscription> examInscriptions;
	private List<CareerInscription> careerInscriptions;
	private List<Inscription> subjectInscriptions;
	private List<SubjectRegularity> subjectRegularities;
	private List<Exam> exams;

	public Student(String id, String name) {
		super(id, name);
	}


	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public List<Inscription> getExamInscriptions() {
		return examInscriptions;
	}

	public void setExamInscriptions(List<Inscription> examInscriptions) {
		this.examInscriptions = examInscriptions;
	}

	public List<CareerInscription> getCareerInscriptions() {
		return careerInscriptions;
	}

	public void setCareerInscriptions(List<CareerInscription> careerInscriptions) {
		this.careerInscriptions = careerInscriptions;
	}

	public List<SubjectRegularity> getSubjectRegularities() {
		return subjectRegularities;
	}

	public void setSubjectRegularities(List<SubjectRegularity> subjectRegularities) {
		this.subjectRegularities = subjectRegularities;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	public List<Inscription> getSubjectInscriptions() {
		return subjectInscriptions;
	}

	public void setSubjectInscriptions(List<Inscription> subjectInscriptions) {
		this.subjectInscriptions = subjectInscriptions;
	}

	public void addSubjectInscription(Inscription inscription) {
		this.getSubjectInscriptions().add(inscription);
	}

	public void addExamInscription(Inscription inscription) {
		this.getExamInscriptions().add(inscription);
	}

}
