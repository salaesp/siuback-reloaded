package com.lui.guarani.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by lui on 19/08/15.
 */
public class AcademicHistorySubject {

    private Exam exam;
    private SubjectRegularity subjectRegularity;

    public SubjectRegularity getSubjectRegularity() {
        return subjectRegularity;
    }

    public void setSubjectRegularity(SubjectRegularity subjectRegularity) {
        this.subjectRegularity = subjectRegularity;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }


    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
