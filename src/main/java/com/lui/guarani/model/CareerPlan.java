package com.lui.guarani.model;

import com.google.common.collect.Maps;
import com.lui.guarani.model.id.CareerPlanID;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by lui on 19/05/15.
 */
public class CareerPlan extends AbstractIdentifiableObject implements IUpdateableModel {

	private Map<String, Subject> subjectMap = Maps.newHashMap();

	private String career;
	private String academicUnit;

	public CareerPlan(String id, String name) {
		super(id, name);
	}

	public void addSubject(Subject subject) {
		this.subjectMap.put(subject.getId(), subject);
		subject.setCareerPlanID(new CareerPlanID(getId(), getCareer(), getAcademicUnit()));
	}

	public void addSubjects(List<Subject> subjects) {
		for (Subject subject : subjects) {
			this.addSubject(subject);
		}
	}

	@Override
	public void update() {
		for (Subject subject : subjectMap.values()) {
			subject.setCareerPlanID(new CareerPlanID(getId(), getCareer(), getAcademicUnit()));
		}
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public Map<String, Subject> getSubjectMap() {
		return subjectMap;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getAcademicUnit() {
		return academicUnit;
	}

	public void setAcademicUnit(String academicUnit) {
		this.academicUnit = academicUnit;
	}


}
