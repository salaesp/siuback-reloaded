package com.lui.guarani.model;

import com.lui.guarani.model.id.SubjectID;

import java.util.Date;

/**
 * Created by lui on 19/05/15.
 */
public class Exam extends AbstractSubjectAwareObject {
	private Double grade;

	public Exam(Date inscriptionDate, String id, SubjectID subject, String name, Double grade) {
		super(inscriptionDate, id, subject, name);
		this.grade = grade;
	}

	public Double getGrade() {
		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}
}
