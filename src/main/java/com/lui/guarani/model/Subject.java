package com.lui.guarani.model;

import com.google.common.collect.Lists;
import com.lui.guarani.model.id.CareerPlanID;
import com.lui.guarani.model.id.SubjectID;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

/**
 * Created by lui on 19/05/15.
 */
public class Subject extends AbstractIdentifiableObject {

	private CareerPlanID careerPlanID;

	private List<InscriptionVariant> examVariants = Lists.newArrayList();

	private List<InscriptionVariant> subjectInscriptionVariants = Lists.newArrayList();


	private List<SubjectID> dependsOn = Lists.newArrayList();
	private List<SubjectID> dependsOnForExam = Lists.newArrayList();

	public Subject(String id, String name) {
		super(id, name);
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public List<SubjectID> getDependsOn() {
		return dependsOn;
	}

	public List<SubjectID> getDependsOnForExam() {
		return dependsOnForExam;
	}

	public void addExamDependency(Subject dependency) {
		dependsOn.add(dependency.getSubjectId());
		dependsOnForExam.add(dependency.getSubjectId());
	}

	public SubjectID getSubjectId(){
		return new SubjectID(this.careerPlanID,this.getId());
	}

	public CareerPlanID getCareerPlanID() {
		return careerPlanID;
	}

	public void setCareerPlanID(CareerPlanID careerPlanID) {
		this.careerPlanID = careerPlanID;
	}

	public List<InscriptionVariant> getExamVariants() {
		return examVariants;
	}

	public void setExamVariants(List<InscriptionVariant> examVariants) {
		this.examVariants = examVariants;
	}

	public List<InscriptionVariant> getSubjectInscriptionVariants() {
		return subjectInscriptionVariants;
	}

	public void setSubjectInscriptionVariants(List<InscriptionVariant> subjectInscriptionVariants) {
		this.subjectInscriptionVariants = subjectInscriptionVariants;
	}
}
