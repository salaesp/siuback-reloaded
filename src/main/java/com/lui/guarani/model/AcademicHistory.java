package com.lui.guarani.model;

import com.google.common.collect.Maps;
import com.lui.guarani.model.id.SubjectID;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Map;

/**
 * Created by lui on 02/06/15.
 */
public class AcademicHistory {

    private Map<SubjectID, AcademicHistorySubject> subjects;

    private Double examPercentage;
    private Double subjectsPercentage;
    private Double fullPercentage;

    public AcademicHistory() {
        subjects = Maps.newHashMap();
    }

    public void addExam(Exam exam) {
        SubjectID subject = exam.getSubject();
        AcademicHistorySubject academicHistorySubject = subjects.get(subject);
        if (academicHistorySubject == null) {
            academicHistorySubject = new AcademicHistorySubject();
            subjects.put(subject, academicHistorySubject);
        }
        academicHistorySubject.setExam(exam);
    }

    public void addRegularity(SubjectRegularity regularity) {
        SubjectID subject = regularity.getSubject();
        AcademicHistorySubject academicHistorySubject = subjects.get(subject);
        if (academicHistorySubject == null) {
            academicHistorySubject = new AcademicHistorySubject();
            subjects.put(subject, academicHistorySubject);
        }
        academicHistorySubject.setSubjectRegularity(regularity);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Map<SubjectID, AcademicHistorySubject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Map<SubjectID, AcademicHistorySubject> subjects) {
        this.subjects = subjects;
    }

    public Double getExamPercentage() {
        return examPercentage;
    }

    public void setExamPercentage(Double examPercentage) {
        this.examPercentage = examPercentage;
    }

    public Double getSubjectsPercentage() {
        return subjectsPercentage;
    }

    public void setSubjectsPercentage(Double subjectsPercentage) {
        this.subjectsPercentage = subjectsPercentage;
    }

    public Double getFullPercentage() {
        return fullPercentage;
    }

    public void setFullPercentage(Double fullPercentage) {
        this.fullPercentage = fullPercentage;
    }
}
