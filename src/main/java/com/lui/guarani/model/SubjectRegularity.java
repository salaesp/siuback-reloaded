package com.lui.guarani.model;

import com.lui.guarani.model.id.SubjectID;

import java.util.Date;

/**
 * Created by lui on 19/05/15.
 */
public class SubjectRegularity extends AbstractDateIdentifiableObject {

	private SubjectID subject;

	public SubjectRegularity(String id, String name, Date date, SubjectID subject) {
		super(id, name, date);
		this.subject = subject;
	}

	public SubjectID getSubject() {
		return subject;
	}

	public void setSubject(SubjectID subject) {
		this.subject = subject;
	}
}