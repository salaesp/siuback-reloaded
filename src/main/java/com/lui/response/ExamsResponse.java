package com.lui.response;

import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Inscription;

import java.util.List;

/**
 * Created by lui on 03/06/15.
 */
public class ExamsResponse {
	private List<Exam> exams;
	private List<Inscription> examInscriptions;

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	public List<Inscription> getExamInscriptions() {
		return examInscriptions;
	}

	public void setExamInscriptions(List<Inscription> examInscriptions) {
		this.examInscriptions = examInscriptions;
	}
}
