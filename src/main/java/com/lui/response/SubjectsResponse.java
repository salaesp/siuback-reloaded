package com.lui.response;

import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;

import java.util.List;

/**
 * Created by lui on 03/06/15.
 */
public class SubjectsResponse {
	private List<SubjectRegularity> subjectRegularities;
	private List<Inscription> subjectInscriptions;
	private List<Subject> availableSubjects;
	private List<Subject> availableExams;

	public List<SubjectRegularity> getSubjectRegularities() {
		return subjectRegularities;
	}

	public void setSubjectRegularities(List<SubjectRegularity> subjectRegularities) {
		this.subjectRegularities = subjectRegularities;
	}

	public List<Inscription> getSubjectInscriptions() {
		return subjectInscriptions;
	}

	public void setSubjectInscriptions(List<Inscription> subjectInscriptions) {
		this.subjectInscriptions = subjectInscriptions;
	}

	public List<Subject> getAvailableSubjects() {
		return availableSubjects;
	}

	public void setAvailableSubjects(List<Subject> availableSubjects) {
		this.availableSubjects = availableSubjects;
	}

	public List<Subject> getAvailableExams() {
		return availableExams;
	}

	public void setAvailableExams(List<Subject> availableExams) {
		this.availableExams = availableExams;
	}
}
