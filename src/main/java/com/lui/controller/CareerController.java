package com.lui.controller;

import com.lui.controller.facade.AppUserFacade;
import com.lui.controller.request.InscriptionRequest;
import com.lui.utils.ControllerUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lui on 02/06/15.
 */
@Controller
@RequestMapping("/academicunit/{academicUnitId}/career/{careerId}/careerplan/{careerPlanId}")
public class CareerController {

	final static Logger LOG = Logger.getLogger(CareerController.class);

	@Autowired
	private AppUserFacade appUserFacade;

	@RequestMapping(value = "/subject", method = RequestMethod.GET)
	@ResponseBody
	public Object getUserAvailableSubjects(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
										   @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
										   @RequestParam(value = "regularities", required = false, defaultValue = "false") boolean
												   includeRegularities,
										   @RequestParam(value = "inscriptions", required = false, defaultValue = "false") boolean
												   includeInscriptions,
										   @PathVariable(value = "academicUnitId") String academicUnitId,
										   @PathVariable(value = "careerId") String careerId,
										   @PathVariable(value = "careerPlanId") String careerPlanId) {
		return appUserFacade.getUserAvailableSubjects(loginKey, basicAuthKey, academicUnitId, careerId, careerPlanId, includeRegularities,
				includeInscriptions);
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public Object getUserAcademicHistory(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
										 @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
										 @PathVariable(value = "academicUnitId") String academicUnitId,
										 @PathVariable(value = "careerId") String careerId,
										 @PathVariable(value = "careerPlanId") String careerPlanId) {
		return appUserFacade.getUserAcademicHistory(loginKey, basicAuthKey, academicUnitId, careerId, careerPlanId);
	}

	@RequestMapping(value = "/exam", method = RequestMethod.GET)
	@ResponseBody
	public Object getUserExams(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
							   @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
							   @RequestParam(value = "inscriptions", required = false, defaultValue = "false") boolean
									   includeInscriptions,
							   @PathVariable(value = "academicUnitId") String academicUnitId,
							   @PathVariable(value = "careerId") String careerId,
							   @PathVariable(value = "careerPlanId") String careerPlanId) {
		return appUserFacade.getUserExams(loginKey, basicAuthKey, academicUnitId, careerId, careerPlanId, includeInscriptions);
	}

	@RequestMapping(value = "/subject/{subjectId}", method = RequestMethod.POST)
	@ResponseBody
	public Object getSubject(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
							 @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
							 @RequestBody InscriptionRequest inscriptionRequest,
							 @PathVariable(value = "academicUnitId") String academicUnitId,
							 @PathVariable(value = "careerId") String careerId,
							 @PathVariable(value = "careerPlanId") String careerPlanId,
							 @PathVariable(value = "subjectId") String subjectId) {
		return appUserFacade.executeInscription(loginKey, basicAuthKey, academicUnitId, careerId, careerPlanId, subjectId, inscriptionRequest);
	}

	@RequestMapping(value = "/subject/{subjectId}/inscription/{inscriptionId}", method = RequestMethod.DELETE)
	@ResponseBody
	public Object deleteInscription(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
									@RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
									@PathVariable(value = "academicUnitId") String academicUnitId,
									@PathVariable(value = "careerId") String careerId,
									@PathVariable(value = "careerPlanId") String careerPlanId,
									@PathVariable(value = "subjectId") String subjectId,
									@PathVariable(value = "inscriptionId") String inscriptionId) {
		return appUserFacade.deleteInscription(loginKey, basicAuthKey, academicUnitId, careerId, careerPlanId, subjectId, inscriptionId);
	}
}
