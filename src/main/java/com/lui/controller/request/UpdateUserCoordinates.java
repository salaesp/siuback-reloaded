package com.lui.controller.request;

/**
 * Created by lui on 10/06/15.
 */
public class UpdateUserCoordinates {
	private String coordinates;
	private String status;
	@Deprecated
	private boolean forcedSafeZone;

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	@Deprecated
	public boolean isForcedSafeZone() {
		return forcedSafeZone;
	}

	@Deprecated
	public void setForcedSafeZone(boolean forcedSafeZone) {
		this.forcedSafeZone = forcedSafeZone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
