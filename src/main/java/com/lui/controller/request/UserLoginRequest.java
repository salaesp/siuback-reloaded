package com.lui.controller.request;

import com.google.common.io.BaseEncoding;
import com.lui.exception.InvalidTokenException;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by lui on 14/05/15.
 */
public class UserLoginRequest {
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public UserLoginRequest(String baseAuthKey) {
		if (!baseAuthKey.contains("Basic ")) {
			throw new InvalidTokenException();
		}
		String keyOnly = StringUtils.remove(baseAuthKey, "Basic ");
		String decodedKey = new String(BaseEncoding.base64().decode(keyOnly));
		username = StringUtils.substringBefore(decodedKey, ":");
		password = StringUtils.substringAfter(decodedKey, ":");
		if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
			throw new InvalidTokenException();
		}
	}
}
