package com.lui.controller;

import com.lui.guarani.app.GuaraniApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by lui on 14/05/15.
 */
@Controller
@RequestMapping("/debug")
public class DebugController {

	@Autowired
	private GuaraniApp guaraniApp;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public String root() {
		return "for debug only";
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public Object getAllAppData() {
		return guaraniApp.getUserMap();
	}

}
