package com.lui.controller;

import com.lui.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by lui on 02/06/15.
 */
@ControllerAdvice
public class ControllerExceptionHandler {


	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorMessage handleUnknown(Exception exception) {
		return new ErrorMessage(ErrorMessage.INTERNAL_ERROR, exception.getLocalizedMessage());
	}

	@ExceptionHandler(InvalidTokenException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ErrorMessage handleTokenError(InvalidTokenException exception) {
		return new ErrorMessage(ErrorMessage.INVALID_TOKEN, exception.getLocalizedMessage());
	}

	@ExceptionHandler(InvalidInscriptionException.class)
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ResponseBody
	public ErrorMessage handleInscriptionError(InvalidInscriptionException exception) {
		return new ErrorMessage(exception.getMessage(), exception.getLocalizedMessage());
	}


	@ExceptionHandler(InvalidCoordinatesException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
	public ErrorMessage handleCoordinatesError(InvalidCoordinatesException exception) {
		return new ErrorMessage(ErrorMessage.NOT_IN_THE_ZONE, exception.getLocalizedMessage());
	}


	@ExceptionHandler(InvalidAccessException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorMessage handleLoginError(InvalidAccessException exception) {
		return new ErrorMessage(ErrorMessage.INVALID_ACCESS, exception.getLocalizedMessage());
	}

}
