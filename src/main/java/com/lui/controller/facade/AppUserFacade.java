package com.lui.controller.facade;

import com.lui.controller.request.InscriptionRequest;
import com.lui.controller.request.UserLoginRequest;
import com.lui.dto.model.*;
import com.lui.dto.transformer.Transformer;
import com.lui.guarani.model.AcademicHistory;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.id.CareerPlanID;
import com.lui.guarani.model.id.SubjectID;
import com.lui.model.NearbyInfo;
import com.lui.model.UserBasicInfo;
import com.lui.operation.Operation;
import com.lui.request.*;
import com.lui.response.ExamsResponse;
import com.lui.response.SubjectsResponse;
import com.lui.response.UserLoginResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class AppUserFacade {

    //Operations
    @Autowired
    private Operation<UserCareerAwareRequest, AcademicHistory> getUserAcademicHistoryOperation;
    @Autowired
    private Operation<GetSubjectsRequest, SubjectsResponse> getUserSubjectsOperation;
    @Autowired
    private Operation<GetExamsRequest, ExamsResponse> getUserExamsOperation;
    @Autowired
    private Operation<UserLoginRequest, UserLoginResponse> loginOperation;
    @Autowired
    private Operation<AddInscriptionRequest, Inscription> inscriptionOperation;
    @Autowired
    private Operation<GetInscriptionRequest, Inscription> deleteInscriptionOperation;
    @Autowired
    private Operation<CoordinatesRequest, Boolean> saveCoordinatesOperation;
    @Autowired
    private Operation<SimpleLoginAwareRequest, UserBasicInfo> getUserBasicInfoOperation;
    @Autowired
    private Operation<SimpleLoginAwareRequest, List<NearbyInfo>> getNearbyColleaguesOperation;

    //Transformers
    @Autowired
    private Transformer<UserBasicInfo, UserBasicInfoDTO> userBasicInfoDTOTransformer;
    @Autowired
    private Transformer<AcademicHistory, AcademicHistoryDTO> academicHistoryDTOTransformer;
    @Autowired
    private Transformer<SubjectsResponse, SubjectsResponseDTO> subjectsResponseDTOTransformer;
    @Autowired
    private Transformer<ExamsResponse, ExamsResponseDTO> examsResponseDTOTransformer;
    @Autowired
    private Transformer<Inscription, InscriptionDTO> inscriptionDTOTransformer;
    @Autowired
    private Transformer<NearbyInfo, NearbyInfoDTO> nearbyInfoDTOTransformer;


    public UserLoginResponse login(String authKey) {
        return loginOperation.execute(new UserLoginRequest(authKey));
    }

    public UserBasicInfoDTO getUserCareerInscriptions(String loginKey, String basicAuthKey) {
        SimpleLoginAwareRequest request = new SimpleLoginAwareRequest(loginKey);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return userBasicInfoDTOTransformer.transform(getUserBasicInfoOperation.execute(request));
    }

    public AcademicHistoryDTO getUserAcademicHistory(String loginKey, String basicAuthKey, String academicUnit, String career, String
            careerPlan) {
        CareerPlanID careerPlanID = new CareerPlanID(careerPlan, career, academicUnit);
        UserCareerAwareRequest request = new UserCareerAwareRequest(loginKey, careerPlanID);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return academicHistoryDTOTransformer.transform(getUserAcademicHistoryOperation.execute(request));
    }

    public SubjectsResponseDTO getUserAvailableSubjects(String loginKey, String basicAuthKey, String academicUnit, String career, String
            careerPlan, boolean includeRegularities, boolean includeInscriptions) {
        CareerPlanID careerPlanID = new CareerPlanID(careerPlan, career, academicUnit);
        GetSubjectsRequest request = new GetSubjectsRequest(loginKey, careerPlanID, includeRegularities, includeInscriptions);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return this.subjectsResponseDTOTransformer.transform(this.getUserSubjectsOperation.execute(request));
    }

    public ExamsResponseDTO getUserExams(String loginKey, String basicAuthKey, String academicUnit, String career, String
            careerPlan, boolean includeInscriptions) {
        CareerPlanID careerPlanID = new CareerPlanID(careerPlan, career, academicUnit);
        GetExamsRequest request = new GetExamsRequest(loginKey, careerPlanID, includeInscriptions);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return this.examsResponseDTOTransformer.transform(this.getUserExamsOperation.execute(request));
    }

    public InscriptionDTO executeInscription(String loginKey, String basicAuthKey, String academicUnitId, String careerId, String careerPlanId,
                                             String subjectId, InscriptionRequest inscriptionRequest) {
        SubjectID subject = new SubjectID(academicUnitId, careerId, careerPlanId, subjectId);
        AddInscriptionRequest request = new AddInscriptionRequest(loginKey, subject, inscriptionRequest
                .getInscriptionAction(), inscriptionRequest.getVariant());
        request.setForcedBasicLoginAuth(basicAuthKey);
        return inscriptionDTOTransformer.transform(inscriptionOperation.execute(request));
    }

    public InscriptionDTO deleteInscription(String loginKey, String basicAuthKey, String academicUnitId, String careerId, String careerPlanId, String subjectId, String inscriptionId) {
        SubjectID subject = new SubjectID(academicUnitId, careerId, careerPlanId, subjectId);
        GetInscriptionRequest request = new GetInscriptionRequest(loginKey, subject, inscriptionId);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return inscriptionDTOTransformer.transform(deleteInscriptionOperation.execute(request));
    }

    public Boolean saveUserCoordinates(String loginKey, String basicAuthKey, String coordinates, String status) {
        CoordinatesRequest request = new CoordinatesRequest(loginKey, coordinates, status);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return saveCoordinatesOperation.execute(request);
    }

    public List<NearbyInfoDTO> getNearbyColleagues(String loginKey, String basicAuthKey, String coordinates, String status) {
        saveUserCoordinates(loginKey, basicAuthKey, coordinates, status);
        return this.getNearbyColleagues(loginKey, basicAuthKey);
    }

    public List<NearbyInfoDTO> getNearbyColleagues(String loginKey, String basicAuthKey) {
        SimpleLoginAwareRequest request = new SimpleLoginAwareRequest(loginKey);
        request.setForcedBasicLoginAuth(basicAuthKey);
        return nearbyInfoDTOTransformer.transform(getNearbyColleaguesOperation.execute(request));
    }
}
