package com.lui.controller;

import com.lui.controller.facade.AppUserFacade;
import com.lui.controller.request.UpdateUserCoordinates;
import com.lui.response.UserLoginResponse;
import com.lui.utils.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lui on 14/05/15.
 */
@Controller
@RequestMapping("/user")
public class UserController {


	@Autowired
	private AppUserFacade appUserFacade;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	public UserLoginResponse login(@RequestHeader(value = "Authorization", required = false) String authKey) {
		return appUserFacade.login(authKey);
	}

	@RequestMapping(value = "/me", method = RequestMethod.GET)
	@ResponseBody
	public Object getUserCareerInscriptions(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
											@RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey) {
		return appUserFacade.getUserCareerInscriptions(loginKey, basicAuthKey);
	}

	@RequestMapping(value = "/me", method = RequestMethod.PUT)
	@ResponseBody
	public Object updateUserCoordinates(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
										@RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
										@RequestBody UpdateUserCoordinates request) {
		appUserFacade.saveUserCoordinates(loginKey, basicAuthKey, request.getCoordinates(), request.getStatus());
		return this.getUserCareerInscriptions(loginKey, basicAuthKey);
	}

	@RequestMapping(value = "/me/nearby", method = RequestMethod.POST)
	@ResponseBody
	public Object getNearbyColleaguesWithCoordinates(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
									  @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey,
									  @RequestBody(required = false) UpdateUserCoordinates request) {
		return appUserFacade.getNearbyColleagues(loginKey, basicAuthKey, request.getCoordinates(), request.getStatus());
	}

	@RequestMapping(value = "/me/nearby", method = RequestMethod.GET)
	@ResponseBody
	public Object getNearbyColleagues(@RequestHeader(value = ControllerUtils.SIUBACK_LOGINKEY, required = false) String loginKey,
									  @RequestHeader(value = ControllerUtils.FORCED_AUTHORIZATION, required = false) String basicAuthKey) {
		return appUserFacade.getNearbyColleagues(loginKey, basicAuthKey);
	}

}
