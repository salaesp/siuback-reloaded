package com.lui.facade;

import com.google.common.collect.Maps;
import com.google.common.io.BaseEncoding;
import com.lui.exception.InvalidAccessException;
import com.lui.guarani.app.GuaraniApp;
import com.lui.guarani.model.Student;
import com.lui.guarani.model.User;
import com.lui.model.Coordinate;
import com.lui.model.UserLoginInfo;
import com.lui.model.Visit;
import com.lui.response.UserLoginResponse;
import com.lui.utils.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by lui on 14/05/15.
 */
@Component
public class UserFacadeMockImpl implements UserFacade {
	@Autowired
	private GuaraniApp guaraniApp;
	private Map<String, UserLoginInfo> usersLogged = Maps.newHashMap();
	private Map<String, String> userIdWithKey = Maps.newHashMap();
	@Value(value = "${max.logintime}")
	private Long maxLoginTime;

	@Override
	public UserLoginResponse login(String username, String password) {
		User guaraniLoggedUser = guaraniApp.login(username, password);
		if (guaraniLoggedUser == null) {
			throw new InvalidAccessException("invalid credentials");
		}
		//checks if the user was already logged in, avoids creating new sessions
		String guaraniLoggedUserId = guaraniLoggedUser.getId();
		if (!this.isLoggedUserById(guaraniLoggedUserId)) {
			long timeInMillis = Calendar.getInstance().getTimeInMillis();
			String key = BaseEncoding.base64().encode((username + password + timeInMillis).getBytes());
			if (!usersLogged.containsKey(key)) {
				usersLogged.put(key, new UserLoginInfo(guaraniLoggedUserId, timeInMillis, key));
				userIdWithKey.put(guaraniLoggedUserId, key);
			}
			return new UserLoginResponse(usersLogged.get(key).getLastKey(), ResponseStatus.SUCCESSFUL);
		} else {
			return new UserLoginResponse(this.getKeyForId(guaraniLoggedUserId), ResponseStatus.SUCCESSFUL);
		}
	}

	@Override
	public boolean isUserLoggedByKey(String key) {
		if (this.usersLogged.containsKey(key)) {
			UserLoginInfo userLoginInfo = this.usersLogged.get(key);
			long currentTime = Calendar.getInstance().getTimeInMillis();
			if (currentTime - userLoginInfo.getLastLogin() > maxLoginTime) {
				this.usersLogged.remove(key);
				this.userIdWithKey.remove(userLoginInfo.getUserId());
			} else {
				userLoginInfo.setLastLogin(currentTime);
				return true;
			}
		}
		return false;
	}

	@Override
	public UserLoginInfo getLoggedUser(String key) {
		if (!isUserLoggedByKey(key)) {
			throw new InvalidAccessException("This should never be called before checking if user is logged");
		}
		return this.usersLogged.get(key);
	}

	@Override
	public void saveCoordinates(String key, Coordinate coordinates, String status) {
		UserLoginInfo loggedUser = this.getLoggedUser(key);
		Visit newVisit = new Visit();
		newVisit.setStatus(status);
		newVisit.setChecked(Calendar.getInstance().getTime());
		newVisit.setCoordinates(coordinates);
		loggedUser.setLastSeen(newVisit);
	}

	@Override
	public UserLoginInfo getUserFor(Student student) {
		return this.getLoggedUser(this.userIdWithKey.get(guaraniApp.getUserFor(student).getId()));
	}

	@Override
	public Student getStudentWithUserId(String userId) {
		return guaraniApp.getStudentWithUserId(userId);
	}

	public boolean isLoggedUserById(String id) {
		if (userIdWithKey.containsKey(id)) {
			return isUserLoggedByKey(userIdWithKey.get(id));
		}
		return false;
	}

	public String getKeyForId(String id) {
		return userIdWithKey.get(id);
	}

}