package com.lui.facade;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.lui.exception.InvalidInscriptionException;
import com.lui.exception.NotFoundException;
import com.lui.guarani.app.GuaraniApp;
import com.lui.guarani.model.*;
import com.lui.guarani.model.id.CareerPlanID;
import com.lui.guarani.model.id.SubjectID;
import com.lui.model.UserLoginInfo;
import com.lui.request.AddInscriptionRequest;
import com.lui.request.GetInscriptionRequest;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.request.UserCareerAwareRequest;
import com.lui.utils.InscriptionAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class GuaraniFacadeImpl implements GuaraniFacade {

    final static Logger LOG = Logger.getLogger(GuaraniFacadeImpl.class);
    @Autowired
    private GuaraniApp guaraniApp;

    @Override
    public AcademicUnit getAcademicUnit(String id) {
        AcademicUnit academicUnit = guaraniApp.getAcademicUnit(id);
        if (academicUnit == null) {
            throw new NotFoundException(String.format("No academic unit with id %s", id));
        }
        return academicUnit;
    }

    @Override
    public Career getCareer(String academicUnitId, String careerId) {
        Career career = guaraniApp.getCareer(academicUnitId, careerId);
        if (career == null) {
            throw new NotFoundException(String.format("No career with id %s", careerId));
        }
        return career;
    }

    @Override
    public CareerPlan getCareerPlan(CareerPlanID careerPlanID) {
        CareerPlan careerPlan = guaraniApp.getCareerPlan(careerPlanID.getAcademicUnit(), careerPlanID.getCareer(),
                careerPlanID.getCareerPlan());
        if (careerPlan == null) {
            throw new NotFoundException(String.format("No careerPlan with id %s", careerPlanID.getCareerPlan()));
        }
        return careerPlan;
    }

    @Override
    public Subject getSubject(SubjectID subjectID) {
        Subject subject = guaraniApp.getSubject(subjectID.getAcademicUnit(), subjectID.getCareer(), subjectID
                .getCareerPlan(), subjectID.getSubject());
        if (subject == null) {
            throw new NotFoundException(String.format("No subject with id %s", subjectID));
        }
        return subject;
    }

    @Override
    public List<CareerInscription> getCareerInscriptionsForUser(SimpleLoginAwareRequest request) {
        return guaraniApp.getUserCareerInscriptions(request.getUserLoginInfo().getUserId());
    }

    @Override
    public List<Inscription> getSubjectInscriptions(final UserCareerAwareRequest request) {
        return this.getSubjectInscriptions(request.getUserLoginInfo().getUserId(), request.getCareerPlanID());
    }

    private List<Inscription> getSubjectInscriptions(String userId, final CareerPlanID careerPlanID) {
        List<Inscription> userSubjectInscriptions = guaraniApp.getUserSubjectInscriptions(userId);
        return FluentIterable.from(userSubjectInscriptions).filter(new Predicate<Inscription>() {
            @Override
            public boolean apply(Inscription subjectRegularity) {
                return subjectRegularity.getSubject().isSameCareerPlan(careerPlanID);
            }
        }).toList();
    }

    @Override
    public List<Inscription> getExamInscriptions(final UserCareerAwareRequest request) {
        return this.getExamInscriptions(request.getUserLoginInfo().getUserId(), request.getCareerPlanID());
    }

    private List<Inscription> getExamInscriptions(String userId, final CareerPlanID careerPlanID) {
        List<Inscription> userExamInscriptions = guaraniApp.getUserExamInscriptions(userId);
        ImmutableList<Inscription> inscriptions = FluentIterable.from(userExamInscriptions).filter(new Predicate<Inscription>() {
            @Override
            public boolean apply(Inscription subjectRegularity) {
                return subjectRegularity.getSubject().isSameCareerPlan(careerPlanID);
            }
        }).toList();
        String join = StringUtils.join(FluentIterable.from(inscriptions).transform(new Function<Inscription, String>() {
            @Override
            public String apply(Inscription inscription) {
                return inscription.getId();
            }
        }).toList(), ",");
        return inscriptions;
    }

    @Override
    public List<Exam> getExams(final UserCareerAwareRequest request) {
        return getExams(request.getUserLoginInfo().getUserId(), request.getCareerPlanID());
    }

    private List<Exam> getExams(String userId, final CareerPlanID careerPlanId) {
        List<Exam> exams = guaraniApp.getUserExams(userId);
        return FluentIterable.from(exams).filter(new Predicate<Exam>() {
            @Override
            public boolean apply(Exam exam) {
                return exam.getSubject().isSameCareerPlan(careerPlanId);
            }
        }).toList();

    }

    @Override
    public List<SubjectRegularity> getSubjectRegularities(final UserCareerAwareRequest request) {
        return getSubjectRegularities(request.getCareerPlanID(), request.getUserLoginInfo());
    }

    private List<SubjectRegularity> getSubjectRegularities(final CareerPlanID careerPlanID, UserLoginInfo loginInfo) {
        List<SubjectRegularity> userSubjectRegularities = guaraniApp.getUserSubjectRegularities(loginInfo.getUserId());
        return FluentIterable.from(userSubjectRegularities).filter(new Predicate<SubjectRegularity>() {
            @Override
            public boolean apply(SubjectRegularity subjectRegularity) {
                return subjectRegularity.getSubject().isSameCareerPlan(careerPlanID);
            }
        }).toList();
    }

    @Override
    public Inscription addSubjectInscription(final AddInscriptionRequest request) {
        if (request.getAction().equals(InscriptionAction.SUBJECT_INSCRIPTION)) {
            Subject subject = this.getSubject(request.getSubjectId());
            Set<SubjectID> regularityIds = FluentIterable.from(this.getSubjectRegularities(request.getSubjectId(), request.getUserLoginInfo()
            )).transform(new Function<SubjectRegularity, SubjectID>() {
                @Override
                public SubjectID apply(SubjectRegularity subjectRegularity) {
                    return subjectRegularity.getSubject();
                }
            }).toSet();

            Optional<InscriptionVariant> inscriptionVariantOptional = Iterables.tryFind(subject.getSubjectInscriptionVariants(), new Predicate<InscriptionVariant>() {
                @Override
                public boolean apply(InscriptionVariant variant) {
                    return request.getVariant().equals(variant.getId());
                }
            });
            if (!inscriptionVariantOptional.isPresent()) {
                String message = "Inscripción inválida, no existe la variante";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }

            Set<SubjectID> subjectInscriptions = FluentIterable.from(this.getSubjectInscriptions(request.getUserLoginInfo().getUserId(), request
                    .getSubjectId())).transform(new Function<Inscription, SubjectID>() {
                @Override
                public SubjectID apply(Inscription exam) {
                    return exam.getSubject();
                }
            }).toSet();

            if (subjectInscriptions.contains(request.getSubjectId())) {
                String message = "Inscripción inválida, ya existe";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            if (regularityIds.contains(request.getSubjectId())) {
                String message = "Inscripción inválida, materia completa";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            if (!regularityIds.containsAll(subject.getDependsOn())) {
                String message = "Inscripción inválida, faltan correlativas";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            return guaraniApp.addSubjectInscription(subject, request.getUserLoginInfo().getUserId(), inscriptionVariantOptional.get());

        }
        String message = "Cannot make inscription, trying to add subject with incorrect action";
        LOG.debug(message);
        throw new InvalidInscriptionException(message);
    }

    @Override
    public Inscription addExamInscription(final AddInscriptionRequest request) {
        if (request.getAction().equals(InscriptionAction.EXAM_INSCRIPTION)) {
            Subject subject = this.getSubject(request.getSubjectId());

            Optional<InscriptionVariant> inscriptionVariantOptional = Iterables.tryFind(subject.getExamVariants(),
                    new Predicate<InscriptionVariant>() {
                        @Override
                        public boolean apply(InscriptionVariant variant) {
                            return request.getVariant().equals(variant.getId());
                        }
                    });
            if (!inscriptionVariantOptional.isPresent()) {
                String message = "Inscripción inválida, no existe la variante";
                ;
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }

            List<SubjectRegularity> userSubjectRegularities = guaraniApp.getUserSubjectRegularities(request.getUserLoginInfo().getUserId());
            Optional<SubjectRegularity> subjectRegularityOptional = Iterables.tryFind(userSubjectRegularities, new Predicate<SubjectRegularity>() {
                @Override
                public boolean apply(SubjectRegularity subjectRegularity) {
                    return subjectRegularity.getSubject().equals(request.getSubjectId());
                }
            });

            Set<SubjectID> exams = FluentIterable.from(this.getExams(request.getUserLoginInfo().getUserId(), request.getSubjectId()
            )).transform(new Function<Exam, SubjectID>() {
                @Override
                public SubjectID apply(Exam exam) {
                    return exam.getSubject();
                }
            }).toSet();

            Set<SubjectID> examInscriptions = FluentIterable.from(this.getExamInscriptions(request.getUserLoginInfo().getUserId(), request.getSubjectId())).transform(new Function<Inscription, SubjectID>() {
                @Override
                public SubjectID apply(Inscription exam) {
                    return exam.getSubject();
                }
            }).toSet();
            if (examInscriptions.contains(request.getSubjectId())) {
                String message = "Inscripción inválida, ya existe";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            if (exams.contains(request.getSubjectId())) {
                String message =  "Inscripción inválida, ya se tomó el examen";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            if (!exams.containsAll(subject.getDependsOnForExam())) {
                String message = "Inscripción inválida, faltan correlativas";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            if (!subjectRegularityOptional.isPresent()) {
                String message = "Inscripción inválida, faltan la regularidad de la materia";
                LOG.debug(message);
                throw new InvalidInscriptionException(message);
            }
            LOG.debug("Inscription made successfully");
            return guaraniApp.addExamInscription(subject, request.getUserLoginInfo().getUserId(), inscriptionVariantOptional.get());

        }
        String message = "Cannot make inscription, trying to add exam with incorrect action";
        LOG.debug(message);
        throw new InvalidInscriptionException(message);
    }

    @Override
    public Inscription getExamInscription(final GetInscriptionRequest request) {
        List<Inscription> examInscriptions = this.getExamInscriptions(request.getUserLoginInfo().getUserId(), new CareerPlanID(request.getSubjectId
                ()));
        Optional<Inscription> inscriptionOptional = Iterables.tryFind(examInscriptions, new Predicate<Inscription>() {
            @Override
            public boolean apply(Inscription inscription) {
                return inscription.getSubject().equals(request.getSubjectId()) && inscription.getId().equals(request.getInscriptionId());
            }
        });
        return inscriptionOptional.isPresent() ? inscriptionOptional.get() : null;
    }

    @Override
    public Inscription getSubjectInscription(final GetInscriptionRequest request) {
        List<Inscription> subjectInscriptions = this.getSubjectInscriptions(request.getUserLoginInfo().getUserId(), new CareerPlanID(request.getSubjectId()));
        Optional<Inscription> inscriptionOptional = Iterables.tryFind(subjectInscriptions, new Predicate<Inscription>() {
            @Override
            public boolean apply(Inscription inscription) {
                return inscription.getSubject().equals(request.getSubjectId()) && inscription.getId().equals(request.getInscriptionId());
            }
        });
        return inscriptionOptional.isPresent() ? inscriptionOptional.get() : null;
    }

    @Override
    public Inscription deleteExamInscription(GetInscriptionRequest request) {
        Inscription examInscription = this.getExamInscription(request);
        guaraniApp.deleteExamInscription(request.getUserLoginInfo().getUserId(), examInscription);
        return examInscription;
    }

    @Override
    public Inscription deleteSubjectInscription(GetInscriptionRequest request) {
        Inscription subjectInscription = this.getSubjectInscription(request);
        guaraniApp.deleteSubjectInscription(request.getUserLoginInfo().getUserId(), subjectInscription);
        return subjectInscription;
    }

    @Override
    public List<Student> getStudentsFor(final SubjectID subject) {
        List<Student> studentsFor = guaraniApp.getStudentsFor(subject);
        List<Student> filtered = Lists.newArrayList();

        for (Student student : studentsFor) {
            if (!Iterables.tryFind(student.getSubjectRegularities(), new Predicate<SubjectRegularity>() {
                @Override
                public boolean apply(SubjectRegularity subjectRegularity) {
                    return subjectRegularity.getSubject().equals(subject);
                }
            }).isPresent()) {
                filtered.add(student);
            }
        }
        return filtered;
    }
}
