package com.lui.service;

import com.lui.model.NearbyInfo;
import com.lui.model.UserLoginInfo;
import com.lui.request.CoordinatesRequest;
import com.lui.response.UserLoginResponse;

import java.util.List;

/**
 * Created by lui on 14/05/15.
 */
public interface UserService {
	UserLoginResponse login(String username, String password);

	boolean isUserLogged(String key);

	UserLoginInfo getLoggedUser(String key);

	Boolean saveCoordinates(CoordinatesRequest request);

	List<NearbyInfo> getNearbyColleagues(String key);
}
