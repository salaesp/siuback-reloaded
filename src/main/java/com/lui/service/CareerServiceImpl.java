package com.lui.service;

import com.lui.facade.GuaraniFacade;
import com.lui.guarani.model.Career;
import com.lui.guarani.model.CareerInscription;
import com.lui.request.SimpleLoginAwareRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class CareerServiceImpl implements CareerService {

	@Autowired
	private GuaraniFacade guaraniFacade;

	public Career getById(String academicUnitId, String careerId) {
		return guaraniFacade.getCareer(academicUnitId, careerId);
	}

	@Override
	public List<CareerInscription> listInscriptions(SimpleLoginAwareRequest request) {
		return guaraniFacade.getCareerInscriptionsForUser(request);
	}
}
