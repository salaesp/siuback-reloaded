package com.lui.service;

import com.lui.guarani.model.Exam;
import com.lui.request.UserCareerAwareRequest;

import java.util.List;

/**
 * Created by lui on 02/06/15.
 */
public interface ExamService extends InscriptionService {

	List<Exam> list(UserCareerAwareRequest request);

}
