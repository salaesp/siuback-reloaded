package com.lui.service;

import com.lui.guarani.model.AcademicHistory;
import com.lui.request.UserCareerAwareRequest;

/**
 * Created by lui on 02/06/15.
 */
public interface AcademicHistoryService {

	AcademicHistory getUserAcademicHistory(UserCareerAwareRequest request);

}
