package com.lui.service;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.lui.debug.*;
import com.lui.guarani.app.AppBuilderResponse;
import com.lui.guarani.app.GuaraniAppHelper;
import com.lui.guarani.model.*;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by lui on 11/08/15.
 */
@Component
public class DebugService {

    @Value("classpath:sistemas.json")
    private Resource academicUnit;
    @Value("classpath:users.json")
    private Resource users;

    private DebugAcademicUnit getAcademicUnitFromJson() throws IOException {
        String content = Resources.toString(academicUnit.getURL(), Charsets.UTF_8);
        Gson serializer = new Gson();
        return serializer.fromJson(content, DebugAcademicUnit.class);
    }

    private List<DebugUser> getUsersFromJson() throws IOException {
        String content = Resources.toString(users.getURL(), Charsets.UTF_8);
        Gson serializer = new Gson();
        Type listType = new TypeToken<List<DebugUser>>() {
        }.getType();
        return serializer.fromJson(content, listType);
    }

    public AcademicUnit getAcademicUnit() {

        DebugAcademicUnit academicUnitFromJson = null;
        try {
            academicUnitFromJson = this.getAcademicUnitFromJson();
        } catch (IOException e) {
            return null;
        }
        AcademicUnit academicUnit = new AcademicUnit(academicUnitFromJson.getCode(), academicUnitFromJson.getName());

        for (DebugCareer careerFromjson : academicUnitFromJson.getCareers()) {
            Career career = new Career(careerFromjson.getCode(), careerFromjson.getName());
            academicUnit.addCareer(career);
            for (DebugCareerPlan planFromJson : careerFromjson.getPlans()) {
                CareerPlan plan = new CareerPlan(planFromJson.getCode(), planFromJson.getName());
                career.addCareerPlan(plan);

                Map<String, Subject> correlativesHelper = Maps.newHashMap();

                for (DebugSubject debugSubject : planFromJson.getSubjects()) {
                    Subject subject = new Subject(debugSubject.getCode(), debugSubject.getName());
                    subject.setExamVariants(this.listToVariants(debugSubject.getExamVariants()));
                    subject.setSubjectInscriptionVariants(this.listToVariants(debugSubject.getSubjectVariants()));
                    correlativesHelper.put(subject.getId(), subject);
                    plan.addSubject(subject);
                }

                for (DebugSubject debugSubject : planFromJson.getSubjects()) {
                    Subject subject = correlativesHelper.get(debugSubject.getCode());
                    if (debugSubject.getCorrelatives() == null) {
                        continue;
                    }
                    for (String correlative : debugSubject.getCorrelatives()) {
                        Subject dependency = correlativesHelper.get(correlative);
                        if (dependency == null) {
                            continue;
                        }
                        subject.addExamDependency(dependency);
                    }
                }
            }
        }

        return academicUnit;
    }

    public List<User> getUsers(AcademicUnit academicUnit) {
        List<DebugUser> usersFromJson = null;
        List<User> users = Lists.newArrayList();
        Date currentDate = Calendar.getInstance().getTime();
        try {
            usersFromJson = this.getUsersFromJson();
        } catch (IOException e) {
            return users;
        }
        for (DebugUser debugUser : usersFromJson) {
            DebugStudent debugStudent = debugUser.getStudent();
            Student student = new Student(debugStudent.getId(), debugStudent.getName());

            // careers
            student.setCareerInscriptions(Lists.<CareerInscription>newArrayList());
            for (DebugStudentCareer debugStudentCareer : debugStudent.getCareers()) {

                String careerInscriptionID = GuaraniAppHelper.getId(CareerInscription.class, currentDate, student, debugStudentCareer);
                CareerInscription career = new CareerInscription(careerInscriptionID, careerInscriptionID, currentDate, debugStudentCareer.getAcademicUnit(), debugStudentCareer.getCareer(), debugStudentCareer.getPlan());

                //exam inscriptions
                student.setExams(Lists.<Exam>newArrayList());
                student.setExamInscriptions(Lists.<Inscription>newArrayList());
                for (DebugExam debugExam : debugStudentCareer.getExams()) {
                    Subject subject = getSubject(academicUnit, debugStudentCareer, debugExam.getCode());
                    //exams
                    String examId = GuaraniAppHelper.getIdForDateIdObject(currentDate, subject, student, Exam.class);
                    Exam exam = new Exam(currentDate, examId, subject.getSubjectId(), examId, debugExam.getGrade());
                    student.getExams().add(exam);

                    //inscriptions
                    Inscription examInscription = new Inscription(currentDate, GuaraniAppHelper.getId(Inscription.class, currentDate, student),
                            subject.getSubjectId(), subject.getExamVariants().iterator().next());
                    student.addExamInscription(examInscription);
                }


                student.setSubjectInscriptions(Lists.<Inscription>newArrayList());
                //subject inscriptions
                List<String> subjectInscriptions = debugStudentCareer.getSubjectInscriptions();
                if (subjectInscriptions != null) {
                    for (String subjectInscription : subjectInscriptions) {
                        Subject subject = getSubject(academicUnit, debugStudentCareer, subjectInscription);
                        Inscription inscription = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, subject, student, Inscription.class), subject.getSubjectId(), subject.getSubjectInscriptionVariants().iterator().next());
                        student.addSubjectInscription(inscription);
                    }
                }

                //subject inscriptions
                student.setSubjectRegularities(Lists.<SubjectRegularity>newArrayList());
                for (String debugReg : debugStudentCareer.getRegularities()) {
                    Subject subject = getSubject(academicUnit, debugStudentCareer, debugReg);
                    String regularityId = GuaraniAppHelper.getIdForDateIdObject(currentDate, subject, student, SubjectRegularity.class);
                    SubjectRegularity regularity = new SubjectRegularity(regularityId, regularityId, currentDate, subject.getSubjectId());
                    student.getSubjectRegularities().add(regularity);

                    Inscription inscription = new Inscription(currentDate, GuaraniAppHelper.getIdForDateIdObject(currentDate, subject, student, Inscription.class), subject.getSubjectId(), subject.getSubjectInscriptionVariants().iterator().next());
                    student.addSubjectInscription(inscription);
                }

                student.getCareerInscriptions().add(career);
            }

            users.add(new User(debugUser.getId(), debugUser.getName(), debugUser.getPassword(), student));
        }
        return users;
    }

    private Subject getSubject(AcademicUnit academicUnit, DebugStudentCareer debugStudentCareer, String debugReg) {
        return academicUnit.getCareers().get(debugStudentCareer.getCareer()).getCareerPlanMap().get(debugStudentCareer.getPlan()).getSubjectMap().get(debugReg);
    }

    private List<InscriptionVariant> listToVariants(List<String> variants) {
        return Lists.newArrayList(FluentIterable.from(variants).transform(new Function<String, InscriptionVariant>() {
            @Override
            public InscriptionVariant apply(String s) {
                return new InscriptionVariant(s, Calendar.getInstance().getTime());
            }
        }));
    }

    public AppBuilderResponse buildGuaraniData() {
        AppBuilderResponse response = new AppBuilderResponse();
        AcademicUnit academicUnit = this.getAcademicUnit();
        response.setAcademicUnitList(Lists.newArrayList(academicUnit));
        response.setUserList(this.getUsers(academicUnit));
        return response;
    }

}
