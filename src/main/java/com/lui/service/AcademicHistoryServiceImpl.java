package com.lui.service;

import com.lui.guarani.model.AcademicHistory;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.request.UserCareerAwareRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class AcademicHistoryServiceImpl implements AcademicHistoryService {

    @Autowired
    private ExamService examService;
    @Autowired
    private SubjectService subjectService;

    @Override
    public AcademicHistory getUserAcademicHistory(UserCareerAwareRequest request) {
        AcademicHistory academicHistory = new AcademicHistory();
        List<SubjectRegularity> subjectRegularities = subjectService.listRegularities(request);
        List<Exam> exams = examService.list(request);
        for (SubjectRegularity subjectRegularity : subjectRegularities) {
            academicHistory.addRegularity(subjectRegularity);
        }
        for (Exam exam : exams) {
            academicHistory.addExam(exam);
        }

        int subjectsCount = subjectService.getSubjectsCount(request);

        academicHistory.setExamPercentage(getPercentage(subjectsCount, exams.size()));
        academicHistory.setSubjectsPercentage(getPercentage(subjectsCount, subjectRegularities.size()));
        academicHistory.setFullPercentage(getPercentage(subjectsCount * 2, exams.size() + subjectRegularities.size()));
        return academicHistory;
    }

    private Double getPercentage(int total, int partial) {
        return (Double.valueOf(partial) * 100) / Double.valueOf(total);

    }
}
