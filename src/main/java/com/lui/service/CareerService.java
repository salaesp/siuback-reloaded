package com.lui.service;

import com.lui.guarani.model.Career;
import com.lui.guarani.model.CareerInscription;
import com.lui.request.SimpleLoginAwareRequest;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
public interface CareerService {
	Career getById(String academicUnitId, String careerId);

	List<CareerInscription> listInscriptions(SimpleLoginAwareRequest request);
}
