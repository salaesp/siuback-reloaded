package com.lui.service;

import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.guarani.model.id.SubjectID;
import com.lui.request.UserCareerAwareRequest;

import java.util.List;

/**
 * Created by lui on 02/06/15.
 */
public interface SubjectService extends InscriptionService {

	Subject getById(SubjectID id);

	List<SubjectRegularity> listRegularities(UserCareerAwareRequest request);

	List<Subject> listSubjects(UserCareerAwareRequest request);

	int getSubjectsCount(UserCareerAwareRequest request);

}
