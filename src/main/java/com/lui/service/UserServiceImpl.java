package com.lui.service;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lui.exception.InvalidAccessException;
import com.lui.exception.InvalidCoordinatesException;
import com.lui.facade.GuaraniFacade;
import com.lui.facade.UserFacade;
import com.lui.guarani.model.CareerInscription;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Student;
import com.lui.model.Coordinate;
import com.lui.model.NearbyInfo;
import com.lui.model.UserLoginInfo;
import com.lui.operation.Operation;
import com.lui.request.CoordinatesRequest;
import com.lui.request.GetSubjectsRequest;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.response.SubjectsResponse;
import com.lui.response.UserLoginResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

/**
 * Created by lui on 14/05/15.
 */
@Component
public class UserServiceImpl implements UserService {
    public static Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserFacade userFacade;
    @Autowired
    private GuaraniFacade guaraniFacade;
    @Value("${default.timethreadhold.minutes}")
    private Long timeThreshold;


    @Autowired
    private Operation<GetSubjectsRequest, SubjectsResponse> getUserSubjectsOperation;
    @Autowired
    private Operation<SimpleLoginAwareRequest, List<CareerInscription>> getUserCareerInscriptionsOperation;

    @Autowired
    private GeoService geoService;

    public UserLoginResponse login(String username, String password) {
        return userFacade.login(username, password);
    }

    @Override
    public boolean isUserLogged(String key) {
        return userFacade.isUserLoggedByKey(key);
    }

    @Override
    public UserLoginInfo getLoggedUser(String key) {
        return userFacade.getLoggedUser(key);
    }

    private UserLoginInfo getUserFor(Student student) {
        return userFacade.getUserFor(student);
    }

    @Override
    public Boolean saveCoordinates(CoordinatesRequest request) {
        Coordinate coordinates = this.geoService.getCoordinates(request.getCoordinates());
        if (!geoService.isItInTheSafeZone(coordinates)) {
            LOG.info("The student is out of the safe zone");
            throw new InvalidCoordinatesException("Coordinates not in the zone");
        }
        userFacade.saveCoordinates(request.getUserLoginInfo().getLastKey(), coordinates, request.getStatus());
        return true;
    }

    @Override
    public List<NearbyInfo> getNearbyColleagues(String key) {
        UserLoginInfo loggedUser = getLoggedUser(key);
        if (loggedUser.getLastSeen() == null || !geoService.isItInTheSafeZone(loggedUser.getLastSeen().getCoordinates())) {
            LOG.info("The student is out of the safe zone");
            throw new InvalidCoordinatesException("Student not in the zone");
        }

        List<CareerInscription> careerInscriptions = getUserCareerInscriptionsOperation.execute(new SimpleLoginAwareRequest(key));
        List<Inscription> subjectInscriptions = Lists.newArrayList();
        for (CareerInscription careerInscription : careerInscriptions) {
            GetSubjectsRequest careerAwareRequest = new GetSubjectsRequest(key, careerInscription.getCareerPlanID(), false, true);
            subjectInscriptions.addAll(getUserSubjectsOperation.execute(careerAwareRequest).getSubjectInscriptions());
        }
        Set<Student> students = Sets.newHashSet();
        for (Inscription subjectInscription : subjectInscriptions) {
            students.addAll(guaraniFacade.getStudentsFor(subjectInscription.getSubject()));
        }

        students.remove(userFacade.getStudentWithUserId(loggedUser.getUserId()));

        ImmutableList<NearbyInfo> safeStudents = FluentIterable.from(students).transform(new Function<Student, NearbyInfo>() {
            @Override
            public NearbyInfo apply(Student student) {
                try {
                    UserLoginInfo userFor = userFacade.getUserFor(student);
                    NearbyInfo nearbyInfo = new NearbyInfo();
                    nearbyInfo.setStudent(student);
                    if(userFor.getLastSeen() == null){
                        return null;
                    }
                    nearbyInfo.setVisit(userFor.getLastSeen());
                    return nearbyInfo;
                } catch (InvalidAccessException e) {
                    LOG.warn(String.format("Student %s never logged before", student.getName()));
                    return null;
                }
            }
        }).filter(Predicates.notNull()).filter(new Predicate<NearbyInfo>() {
                                                   @Override
                                                   public boolean apply(NearbyInfo nearbyInfo) {
                                                       if (itsInTime(nearbyInfo)) {
                                                           return false;
                                                       }
                                                       return itsInTheSafeZone(nearbyInfo);
                                                   }
                                               }

        ).toList();

        return safeStudents;
    }

    private boolean itsInTime(NearbyInfo nearbyInfo) {
        return Calendar.getInstance().getTime().getTime() - nearbyInfo.getVisit().getChecked().getTime() > (timeThreshold * 1000 * 60);
    }

    private boolean itsInTheSafeZone(NearbyInfo nearbyInfo) {
        return geoService.isItInTheSafeZone(nearbyInfo.getVisit().getCoordinates());
    }

}
