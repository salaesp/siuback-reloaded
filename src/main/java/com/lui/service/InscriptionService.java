package com.lui.service;

import com.lui.guarani.model.Inscription;
import com.lui.request.AddInscriptionRequest;
import com.lui.request.GetInscriptionRequest;
import com.lui.request.UserCareerAwareRequest;

import java.util.List;

/**
 * Created by lui on 06/06/15.
 */
public interface InscriptionService {

	List<Inscription> listInscriptions(UserCareerAwareRequest request);

	Inscription addInscription(AddInscriptionRequest request);

	Inscription getInscription(GetInscriptionRequest request);

	Inscription deleteInscription(GetInscriptionRequest request);

}
