package com.lui.service;

import com.lui.facade.GuaraniFacadeImpl;
import com.lui.guarani.model.AcademicUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class AcademicUnitServiceImpl implements AcademicUnitService {

	@Autowired
	private GuaraniFacadeImpl guaraniFacade;

	public AcademicUnit getById(String id) {
		return guaraniFacade.getAcademicUnit(id);
	}
}
