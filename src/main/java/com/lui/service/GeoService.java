package com.lui.service;

import com.lui.model.Coordinate;

/**
 * Created by lui on 09/06/15.
 */
public interface GeoService {

	Coordinate getCoordinates(String coordinates);

	boolean isItInTheSafeZone(Coordinate coordinate);
}
