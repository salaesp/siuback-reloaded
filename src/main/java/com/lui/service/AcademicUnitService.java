package com.lui.service;

import com.lui.guarani.model.AcademicUnit;

/**
 * Created by lui on 31/05/15.
 */
public interface AcademicUnitService {

	AcademicUnit getById(String id);

}
