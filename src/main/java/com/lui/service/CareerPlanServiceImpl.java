package com.lui.service;

import com.lui.facade.GuaraniFacade;
import com.lui.guarani.model.CareerPlan;
import com.lui.guarani.model.id.CareerPlanID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class CareerPlanServiceImpl implements CareerPlanService {

	@Autowired
	private GuaraniFacade guaraniFacade;

	public CareerPlan getById(CareerPlanID careerPlanID) {
		return guaraniFacade.getCareerPlan(careerPlanID);
	}
}
