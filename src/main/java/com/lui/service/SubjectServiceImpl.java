package com.lui.service;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.lui.facade.GuaraniFacade;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.guarani.model.id.SubjectID;
import com.lui.request.AddInscriptionRequest;
import com.lui.request.GetInscriptionRequest;
import com.lui.request.UserCareerAwareRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class SubjectServiceImpl implements SubjectService {

	@Autowired
	private GuaraniFacade guaraniFacade;
	@Autowired
	private CareerPlanService careerPlanService;

	@Override
	public Subject getById(SubjectID subjectID) {
		return guaraniFacade.getSubject(subjectID);
	}

	@Override
	public List<Inscription> listInscriptions(UserCareerAwareRequest request) {
		List<Inscription> subjectInscriptions = guaraniFacade.getSubjectInscriptions(request);

		final Set<SubjectID> subjectIDs = FluentIterable.from(this.listRegularities(request)).transform(new Function<SubjectRegularity, SubjectID>() {
			@Override
			public SubjectID apply(SubjectRegularity exam) {
				return exam.getSubject();
			}
		}).toSet();

		return FluentIterable.from(subjectInscriptions).filter(new Predicate<Inscription>() {
			@Override
			public boolean apply(Inscription subjectInscription) {
				return !subjectIDs.contains(subjectInscription.getSubject());
			}
		}).toList();
	}

	@Override
	public List<SubjectRegularity> listRegularities(UserCareerAwareRequest request) {
		return guaraniFacade.getSubjectRegularities(request);
	}

	@Override
	public List<Subject> listSubjects(UserCareerAwareRequest request) {
		List<Subject> subjects = Lists.newArrayList(careerPlanService.getById(request.getCareerPlanID())
				.getSubjectMap().values());
		final Set<String> allInscriptions = FluentIterable.from(guaraniFacade.getSubjectInscriptions(request))
				.transform(new Function<Inscription, String>() {
					@Override
					public String apply(Inscription subjectInscription) {
						return subjectInscription.getSubject().getSubject();
					}
				}).toSet();
		return FluentIterable.from(subjects).filter(new Predicate<Subject>() {
			@Override
			public boolean apply(Subject subject) {
				return !allInscriptions.contains(subject.getId());
			}
		}).toList();
	}

	@Override
	public int getSubjectsCount(UserCareerAwareRequest request) {
		List<Subject> subjects = Lists.newArrayList(careerPlanService.getById(request.getCareerPlanID())
				.getSubjectMap().values());
		return subjects.size();
	}

	@Override
	public Inscription addInscription(AddInscriptionRequest request) {
		return guaraniFacade.addSubjectInscription(request);
	}

	@Override
	public Inscription getInscription(GetInscriptionRequest request) {
		return guaraniFacade.getSubjectInscription(request);
	}

	@Override
	public Inscription deleteInscription(GetInscriptionRequest request) {
		return guaraniFacade.deleteSubjectInscription(request);
	}
}
