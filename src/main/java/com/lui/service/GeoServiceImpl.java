package com.lui.service;

import com.lui.exception.InvalidCoordinatesException;
import com.lui.model.Coordinate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * Created by lui on 10/06/15.
 */
@Component
public class GeoServiceImpl implements GeoService {

	@Value("${default.university.coordinates}")
	private String referenceCoordinates;
	//en metros
	@Value("${default.university.maxDistance}")
	private int distanceToReference;

	@Value("${valid.coordinates.regex}")
	private String coordinatesRegex;

	/**
	 * Returns distance from a to b in meters
	 *
	 * @return
	 */
	private double distance(Coordinate a, Coordinate b) {
		double lat1 = a.getLatitude();
		double lon1 = a.getLongitude();
		double lat2 = b.getLatitude();
		double lon2 = b.getLongitude();

		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		dist = dist * 1.609344 * 1000;
		return (dist);
	}

	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	@Override
	public Coordinate getCoordinates(String coordinates) {
		Pattern pattern = Pattern.compile(coordinatesRegex);
		if (!pattern.matcher(coordinates).matches()) {
			throw new InvalidCoordinatesException(String.format("Coordinates %s are not valid", coordinates));
		}
		return stringToCoordinates(coordinates);
	}

	private Coordinate stringToCoordinates(String coordinates) {
		Double latitude = Double.valueOf(StringUtils.substringBefore(coordinates, ","));
		Double longitude = Double.valueOf(StringUtils.substringAfter(coordinates, ","));
		return new Coordinate(latitude, longitude);
	}

	@Override
	public boolean isItInTheSafeZone(Coordinate coordinate) {
		return this.distance(coordinate, this.stringToCoordinates(this.referenceCoordinates)) < distanceToReference;
	}
}
