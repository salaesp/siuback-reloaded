package com.lui.model;

import com.lui.guarani.model.Student;

/**
 * Created by lui on 20/08/15.
 */
public class NearbyInfo {
    private Student student;
    private Visit visit;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }
}
