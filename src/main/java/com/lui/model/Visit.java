package com.lui.model;

import java.util.Date;

/**
 * Created by lui on 10/06/15.
 */
public class Visit {

	private String status;
	private Date checked;
	private Coordinate coordinates;

	public Date getChecked() {
		return checked;
	}

	public void setChecked(Date checked) {
		this.checked = checked;
	}

	public Coordinate getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinate coordinates) {
		this.coordinates = coordinates;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
