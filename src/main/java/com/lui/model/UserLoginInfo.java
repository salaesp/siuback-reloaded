package com.lui.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by lui on 14/05/15.
 */
public class UserLoginInfo {
	private long lastLogin;
	private String lastKey;
	private String userId;
	private Visit lastSeen;

	public UserLoginInfo(String userId, long lastLogin, String lastKey) {
		this.lastLogin = lastLogin;
		this.lastKey = lastKey;
		this.userId = userId;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getLastKey() {
		return lastKey;
	}

	public void setLastKey(String lastKey) {
		this.lastKey = lastKey;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Visit getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Visit lastSeen) {
		this.lastSeen = lastSeen;
	}
}
