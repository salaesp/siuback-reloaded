package com.lui.dto.model;

import java.util.Date;

/**
 * Created by lui on 08/06/15.
 */
public class InscriptionVariantDTO {
	private String id;
	private Date date;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
