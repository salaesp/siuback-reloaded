package com.lui.dto.model;

import java.util.List;

/**
 * Created by lui on 10/06/15.
 */
public class UserBasicInfoDTO {
	private List<CareerInscriptionDTO> careers;
	private VisitDTO lastSeen;

	public List<CareerInscriptionDTO> getCareers() {
		return careers;
	}

	public void setCareers(List<CareerInscriptionDTO> careers) {
		this.careers = careers;
	}

	public VisitDTO getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(VisitDTO lastSeen) {
		this.lastSeen = lastSeen;
	}
}
