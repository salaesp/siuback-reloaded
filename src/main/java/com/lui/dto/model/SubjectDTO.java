package com.lui.dto.model;

import java.util.List;

/**
 * Created by lui on 02/06/15.
 */
public class SubjectDTO extends AbstractIdentifiableObjectDTO {
	private List<InscriptionVariantDTO> examVariants;
	private List<InscriptionVariantDTO> subjectInscriptionVariants;

	public List<InscriptionVariantDTO> getExamVariants() {
		return examVariants;
	}

	public void setExamVariants(List<InscriptionVariantDTO> examVariants) {
		this.examVariants = examVariants;
	}

	public List<InscriptionVariantDTO> getSubjectInscriptionVariants() {
		return subjectInscriptionVariants;
	}

	public void setSubjectInscriptionVariants(List<InscriptionVariantDTO> subjectInscriptionVariants) {
		this.subjectInscriptionVariants = subjectInscriptionVariants;
	}
}
