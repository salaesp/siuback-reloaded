package com.lui.dto.model;

/**
 * Created by lui on 20/08/15.
 */
public class NearbyInfoDTO {
    private StudentDTO student;
    private String status;
    private CoordinateDTO coordinate;

    public CoordinateDTO getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(CoordinateDTO coordinate) {
        this.coordinate = coordinate;
    }

    public StudentDTO getStudent() {
        return student;
    }

    public void setStudent(StudentDTO student) {
        this.student = student;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
