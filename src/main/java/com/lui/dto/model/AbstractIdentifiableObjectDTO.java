package com.lui.dto.model;

/**
 * Created by lui on 31/05/15.
 */
public abstract class AbstractIdentifiableObjectDTO {
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
