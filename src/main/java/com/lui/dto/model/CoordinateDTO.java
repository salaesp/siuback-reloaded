package com.lui.dto.model;

/**
 * Created by lui on 20/08/15.
 */
public class CoordinateDTO {
    private Double longitude;
    private Double latitude;

    public CoordinateDTO(Double latitude, Double longitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
