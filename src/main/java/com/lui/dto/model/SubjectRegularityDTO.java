package com.lui.dto.model;

/**
 * Created by lui on 02/06/15.
 */
public class SubjectRegularityDTO extends AbstractDateIdentifiableObjectDTO {
	private SubjectDTO subject;

	public SubjectDTO getSubject() {
		return subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}
}
