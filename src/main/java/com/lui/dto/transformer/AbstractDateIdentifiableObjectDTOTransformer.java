package com.lui.dto.transformer;

import com.lui.dto.model.AbstractDateIdentifiableObjectDTO;
import com.lui.guarani.model.AbstractDateIdentifiableObject;

/**
 * Created by lui on 31/05/15.
 */
public abstract class AbstractDateIdentifiableObjectDTOTransformer<I extends AbstractDateIdentifiableObject, O extends AbstractDateIdentifiableObjectDTO> extends AbstractIdentifiableTransformerDTOTransformer<I, O> {

	@Override
	public O transform(I in) {
		O out = super.transform(in);
		out.setDate(in.getDate());
		return out;
	}
}
