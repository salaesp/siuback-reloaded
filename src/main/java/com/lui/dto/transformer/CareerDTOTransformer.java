package com.lui.dto.transformer;

import com.lui.dto.model.CareerDTO;
import com.lui.guarani.model.Career;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class CareerDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<Career, CareerDTO> {
	@Override
	protected void transform(Career input, CareerDTO output) {

	}

	@Override
	protected CareerDTO getInstance() {
		return new CareerDTO();
	}
}
