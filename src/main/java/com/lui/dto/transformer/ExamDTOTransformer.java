package com.lui.dto.transformer;

import com.lui.dto.model.ExamDTO;
import com.lui.dto.model.SubjectDTO;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Subject;
import com.lui.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class ExamDTOTransformer extends AbstractDateIdentifiableObjectDTOTransformer<Exam, ExamDTO> {

	@Autowired
	private SubjectService subjectService;
	@Autowired
	private Transformer<Subject, SubjectDTO> subjectDTOTransformer;

	@Override
	protected void transform(Exam input, ExamDTO output) {
		output.setGrade(input.getGrade());
		output.setSubject(subjectDTOTransformer.transform(subjectService.getById(input.getSubject())));
	}

	@Override
	protected ExamDTO getInstance() {
		return new ExamDTO();
	}
}
