package com.lui.dto.transformer;

import com.lui.dto.model.InscriptionDTO;
import com.lui.dto.model.SubjectDTO;
import com.lui.dto.model.SubjectRegularityDTO;
import com.lui.dto.model.SubjectsResponseDTO;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.response.SubjectsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 03/06/15.
 */
@Component
public class SubjectsResponseDTOTransformer extends AbstractTransformer<SubjectsResponse, SubjectsResponseDTO> {

	@Autowired
	private Transformer<Subject, SubjectDTO> subjectDTOTransformer;
	@Autowired
	private Transformer<SubjectRegularity, SubjectRegularityDTO> subjectRegularityDTOTransformer;
	@Autowired
	private Transformer<Inscription, InscriptionDTO> subjectInscriptionDTOTransformer;

	@Override
	public SubjectsResponseDTO transform(SubjectsResponse in) {
		SubjectsResponseDTO out = new SubjectsResponseDTO();
		out.setAvailableSubjects(subjectDTOTransformer.transform(in.getAvailableSubjects()));
		out.setSubjectRegularities(subjectRegularityDTOTransformer.transform(in.getSubjectRegularities()));
		out.setSubjectInscriptions(subjectInscriptionDTOTransformer.transform(in.getSubjectInscriptions()));
		out.setAvailableExams(subjectDTOTransformer.transform(in.getAvailableExams()));
		return out;
	}
}
