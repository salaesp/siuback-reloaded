package com.lui.dto.transformer;

import com.lui.dto.model.CareerInscriptionDTO;
import com.lui.dto.model.UserBasicInfoDTO;
import com.lui.dto.model.VisitDTO;
import com.lui.guarani.model.CareerInscription;
import com.lui.model.UserBasicInfo;
import com.lui.model.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 10/06/15.
 */
@Component
public class UserBasicInfoDTOTransformer extends AbstractTransformer<UserBasicInfo, UserBasicInfoDTO> {
	@Autowired
	private Transformer<CareerInscription, CareerInscriptionDTO> careerInscriptionDTOTransformer;
	@Autowired
	private Transformer<Visit, VisitDTO> visitDTOTransformer;

	@Override
	public UserBasicInfoDTO transform(UserBasicInfo in) {
		UserBasicInfoDTO out = new UserBasicInfoDTO();
		out.setCareers(careerInscriptionDTOTransformer.transform(in.getCareerInscriptions()));
		if (in.getLastSeen() != null) {
			out.setLastSeen(visitDTOTransformer.transform(in.getLastSeen()));
		}
		return out;
	}
}
