package com.lui.dto.transformer;

import com.lui.dto.model.CareerPlanDTO;
import com.lui.guarani.model.CareerPlan;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class CareerPlanDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<CareerPlan, CareerPlanDTO> {
	@Override
	protected void transform(CareerPlan input, CareerPlanDTO output) {

	}

	@Override
	protected CareerPlanDTO getInstance() {
		return new CareerPlanDTO();
	}
}
