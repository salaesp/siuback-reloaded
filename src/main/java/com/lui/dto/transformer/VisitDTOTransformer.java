package com.lui.dto.transformer;

import com.lui.dto.model.VisitDTO;
import com.lui.model.Visit;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 10/06/15.
 */
@Component
public class VisitDTOTransformer extends AbstractTransformer<Visit, VisitDTO> {
	@Override
	public VisitDTO transform(Visit in) {
		String coordinates = in.getCoordinates().getLatitude().toString() + ", " + in.getCoordinates().getLongitude().toString();
		VisitDTO out = new VisitDTO();
		out.setCoordinates(coordinates);
		out.setLastChecked(in.getChecked());
		return out;
	}
}
