package com.lui.dto.transformer;

import com.lui.dto.model.AcademicHistoryDTO;
import com.lui.dto.model.AcademicHistorySubjectDTO;
import com.lui.dto.model.ExamDTO;
import com.lui.dto.model.SubjectRegularityDTO;
import com.lui.guarani.model.AcademicHistory;
import com.lui.guarani.model.AcademicHistorySubject;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.SubjectRegularity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class AcademicHistoryDTOTransformer extends AbstractTransformer<AcademicHistory, AcademicHistoryDTO> {

    @Autowired
    private Transformer<Exam, ExamDTO> examDTOTransformer;
    @Autowired
    private Transformer<SubjectRegularity, SubjectRegularityDTO> subjectRegularityDTOTransformer;

    @Override
    public AcademicHistoryDTO transform(AcademicHistory in) {
        AcademicHistoryDTO out = new AcademicHistoryDTO();
        for (AcademicHistorySubject subject : in.getSubjects().values()) {
            AcademicHistorySubjectDTO subjectDTO = new AcademicHistorySubjectDTO();
            Exam exam = subject.getExam();
            if (exam != null) {
                subjectDTO.setExam(examDTOTransformer.transform(exam));
            }
            subjectDTO.setSubjectRegularity(subjectRegularityDTOTransformer.transform(subject.getSubjectRegularity()));
            out.getSubjects().add(subjectDTO);
        }
        out.setExamPercentage(in.getExamPercentage());
        out.setFullPercentage(in.getFullPercentage());
        out.setSubjectsPercentage(in.getSubjectsPercentage());
        return out;
    }
}
