package com.lui.dto.transformer;

import com.lui.dto.model.InscriptionVariantDTO;
import com.lui.guarani.model.InscriptionVariant;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 08/06/15.
 */
@Component
public class InscriptionVariantDTOTransformer extends AbstractTransformer<InscriptionVariant, InscriptionVariantDTO> {
	@Override
	public InscriptionVariantDTO transform(InscriptionVariant in) {
		InscriptionVariantDTO out = new InscriptionVariantDTO();
		out.setId(in.getId());
		out.setDate(in.getDate());
		return out;
	}
}
