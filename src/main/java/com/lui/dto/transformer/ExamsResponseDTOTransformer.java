package com.lui.dto.transformer;

import com.lui.dto.model.ExamDTO;
import com.lui.dto.model.ExamsResponseDTO;
import com.lui.dto.model.InscriptionDTO;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Inscription;
import com.lui.response.ExamsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 04/06/15.
 */
@Component
public class ExamsResponseDTOTransformer extends AbstractTransformer<ExamsResponse, ExamsResponseDTO> {

	@Autowired
	private Transformer<Exam, ExamDTO> examDTOTransformer;
	@Autowired
	private Transformer<Inscription, InscriptionDTO> examInscriptionDTOTransformer;

	@Override
	public ExamsResponseDTO transform(ExamsResponse in) {
		ExamsResponseDTO out = new ExamsResponseDTO();
		out.setExamInscriptions(examInscriptionDTOTransformer.transform(in.getExamInscriptions()));
		out.setExams(examDTOTransformer.transform(in.getExams()));
		return out;
	}
}
