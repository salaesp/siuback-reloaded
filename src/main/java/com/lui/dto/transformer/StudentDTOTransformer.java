package com.lui.dto.transformer;

import com.lui.dto.model.StudentDTO;
import com.lui.guarani.model.Student;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 14/06/15.
 */
@Component
public class StudentDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<Student, StudentDTO> {
	@Override
	protected void transform(Student input, StudentDTO output) {

	}

	@Override
	protected StudentDTO getInstance() {
		return new StudentDTO();
	}
}
