package com.lui.dto.transformer;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
public abstract class AbstractTransformer<I, O> implements Transformer<I, O> {

	@Override
	public List<O> transform(List<I> in) {
		if (in == null) {
			return null;
		}
		return Lists.newArrayList(FluentIterable.from(in).transform(new Function<I, O>() {
			@Override
			public O apply(I i) {
				return transform(i);
			}
		}).toList());
	}
}
