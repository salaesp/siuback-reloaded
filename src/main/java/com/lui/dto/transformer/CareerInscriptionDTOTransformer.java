package com.lui.dto.transformer;

import com.lui.dto.model.AcademicUnitDTO;
import com.lui.dto.model.CareerDTO;
import com.lui.dto.model.CareerInscriptionDTO;
import com.lui.dto.model.CareerPlanDTO;
import com.lui.guarani.model.AcademicUnit;
import com.lui.guarani.model.Career;
import com.lui.guarani.model.CareerInscription;
import com.lui.guarani.model.CareerPlan;
import com.lui.service.AcademicUnitService;
import com.lui.service.CareerPlanService;
import com.lui.service.CareerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class CareerInscriptionDTOTransformer extends AbstractDateIdentifiableObjectDTOTransformer<CareerInscription, CareerInscriptionDTO> {

	@Autowired
	private AcademicUnitService academicUnitService;
	@Autowired
	private CareerService careerService;
	@Autowired
	private CareerPlanService careerPlanService;

	@Autowired
	private Transformer<Career, CareerDTO> careerDTOTransformer;
	@Autowired
	private Transformer<CareerPlan, CareerPlanDTO> careerPlanDTOTransformer;
	@Autowired
	private Transformer<AcademicUnit, AcademicUnitDTO> academicUnitDTOTransformer;

	@Override
	protected void transform(CareerInscription input, CareerInscriptionDTO output) {
		output.setAcademicUnit(academicUnitDTOTransformer.transform(academicUnitService.getById(input
				.getCareerPlanID().getAcademicUnit())));
		output.setCareer(careerDTOTransformer.transform(careerService.getById(input.getCareerPlanID().getAcademicUnit(), input.getCareerPlanID().getCareer())));
		output.setCareerPlan(careerPlanDTOTransformer.transform(careerPlanService.getById(input.getCareerPlanID())));
	}

	@Override
	protected CareerInscriptionDTO getInstance() {
		return new CareerInscriptionDTO();
	}
}
