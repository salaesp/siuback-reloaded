package com.lui.dto.transformer;

import com.lui.dto.model.InscriptionVariantDTO;
import com.lui.dto.model.SubjectDTO;
import com.lui.guarani.model.InscriptionVariant;
import com.lui.guarani.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class SubjectDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<Subject, SubjectDTO> {

	@Autowired
	private Transformer<InscriptionVariant, InscriptionVariantDTO> inscriptionVariantDTOTransformer;

	@Override
	protected void transform(Subject input, SubjectDTO out) {
		out.setExamVariants(inscriptionVariantDTOTransformer.transform(input.getExamVariants()));
		out.setSubjectInscriptionVariants(inscriptionVariantDTOTransformer.transform(input.getSubjectInscriptionVariants()));
	}

	@Override
	protected SubjectDTO getInstance() {
		return new SubjectDTO();
	}
}
