package com.lui.dto.transformer;

import com.lui.dto.model.InscriptionDTO;
import com.lui.dto.model.InscriptionVariantDTO;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.InscriptionVariant;
import com.lui.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class InscriptionDTOTransformer extends AbstractDateIdentifiableObjectDTOTransformer<Inscription, InscriptionDTO> {
	@Autowired
	private Transformer<InscriptionVariant, InscriptionVariantDTO> inscriptionVariantDTOTransformer;

	@Autowired
	private SubjectService subjectService;
	@Autowired
	private SubjectDTOTransformer subjectDTOTransformer;

	@Override
	protected void transform(Inscription in, InscriptionDTO out) {
		out.setVariant(inscriptionVariantDTOTransformer.transform(in.getVariant()));
		out.setSubject(subjectDTOTransformer.transform(subjectService.getById(in.getSubject())));
	}

	@Override
	protected InscriptionDTO getInstance() {
		return new InscriptionDTO();
	}
}
