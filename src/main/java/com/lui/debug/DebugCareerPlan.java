package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 11/08/15.
 */
public class DebugCareerPlan {

    private List<DebugSubject> subjects;
    private String code;
    private String name;


    public List<DebugSubject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<DebugSubject> subjects) {
        this.subjects = subjects;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
