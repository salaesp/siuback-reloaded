package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 11/08/15.
 */
public class DebugCareer {

    private List<DebugCareerPlan> plans;
    private String code;
    private String name;

    public List<DebugCareerPlan> getPlans() {
        return plans;
    }

    public void setPlans(List<DebugCareerPlan> plans) {
        this.plans = plans;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
