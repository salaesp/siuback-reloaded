package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 11/08/15.
 */
public class DebugSubject {

    private String code;
    private String name;
    private String period;
    private List<String> correlatives;
    private List<String> examVariants;
    private List<String> subjectVariants;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCorrelatives() {
        return correlatives;
    }

    public void setCorrelatives(List<String> correlatives) {
        this.correlatives = correlatives;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public List<String> getExamVariants() {
        return examVariants;
    }

    public void setExamVariants(List<String> examVariants) {
        this.examVariants = examVariants;
    }

    public List<String> getSubjectVariants() {
        return subjectVariants;
    }

    public void setSubjectVariants(List<String> subjectVariants) {
        this.subjectVariants = subjectVariants;
    }
}
