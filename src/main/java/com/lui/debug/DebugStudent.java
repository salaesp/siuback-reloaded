package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 16/08/15.
 */
public class DebugStudent {

    private String name;
    private String id;
    private List<DebugStudentCareer> careers;

    public List<DebugStudentCareer> getCareers() {
        return careers;
    }

    public void setCareers(List<DebugStudentCareer> careers) {
        this.careers = careers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
