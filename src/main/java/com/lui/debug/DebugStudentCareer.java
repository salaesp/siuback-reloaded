package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 19/08/15.
 */
public class DebugStudentCareer {
    private String academicUnit;
    private String career;
    private String plan;
    private List<String> regularities;
    private List<String> subjectInscriptions;
    private List<DebugExam> exams;

    public String getAcademicUnit() {
        return academicUnit;
    }

    public void setAcademicUnit(String academicUnit) {
        this.academicUnit = academicUnit;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public List<String> getRegularities() {
        return regularities;
    }

    public void setRegularities(List<String> regularities) {
        this.regularities = regularities;
    }

    public List<DebugExam> getExams() {
        return exams;
    }

    public void setExams(List<DebugExam> exams) {
        this.exams = exams;
    }

    public List<String> getSubjectInscriptions() {
        return subjectInscriptions;
    }

    public void setSubjectInscriptions(List<String> subjectInscriptions) {
        this.subjectInscriptions = subjectInscriptions;
    }
}
