package com.lui.operation;

import com.lui.request.GetExamsRequest;
import com.lui.response.ExamsResponse;
import com.lui.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class GetUserExamsOperation extends AbstractLoginAwareOperation<GetExamsRequest, ExamsResponse> {

	@Autowired
	private ExamService examService;

	@Override
	protected ExamsResponse doExecute(GetExamsRequest request) {
		ExamsResponse examsResponse = new ExamsResponse();
		examsResponse.setExams(examService.list(request));
		if (request.isIncludeInscriptions()) {
			examsResponse.setExamInscriptions(examService.listInscriptions(request));
		}
		return examsResponse;
	}
}
