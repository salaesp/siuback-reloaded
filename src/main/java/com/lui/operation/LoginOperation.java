package com.lui.operation;

import com.lui.controller.request.UserLoginRequest;
import com.lui.response.UserLoginResponse;
import com.lui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 04/06/15.
 */
@Component
public class LoginOperation implements Operation<UserLoginRequest, UserLoginResponse> {

	@Autowired
	private UserService userService;

	@Override
	public UserLoginResponse execute(UserLoginRequest request) {
		return userService.login(request.getUsername(), request.getPassword());
	}
}
