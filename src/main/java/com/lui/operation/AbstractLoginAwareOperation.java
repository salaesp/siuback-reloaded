package com.lui.operation;

import com.lui.controller.request.UserLoginRequest;
import com.lui.exception.InvalidTokenException;
import com.lui.request.AbstractLoginAwareRequest;
import com.lui.response.UserLoginResponse;
import com.lui.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by lui on 28/05/15.
 */
public abstract class AbstractLoginAwareOperation<I extends AbstractLoginAwareRequest, O> implements Operation<I, O> {

	@Autowired
	private UserService userService;
	@Autowired
	private Operation<UserLoginRequest, UserLoginResponse> loginOperation;

	@Override
	public O execute(I request) {
		String loginKey;
		if (!StringUtils.isEmpty(request.getForcedBasicLoginAuth())) {
			UserLoginResponse userLoginResponse = loginOperation.execute(new UserLoginRequest(request.getForcedBasicLoginAuth()));
			loginKey = userLoginResponse.getLoginKey();
		} else {
			loginKey = request.getLoginKey();
		}
		if (!userService.isUserLogged(loginKey)) {
			throw new InvalidTokenException();
		}
		request.setUserLoginInfo(userService.getLoggedUser(loginKey));
		request.setLoginKey(loginKey);
		return this.doExecute(request);
	}

	protected abstract O doExecute(I request);

}
