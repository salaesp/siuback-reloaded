package com.lui.operation;

import com.lui.request.CoordinatesRequest;
import com.lui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 10/06/15.
 */
@Component
public class SaveCoordinatesOperation extends AbstractLoginAwareOperation<CoordinatesRequest, Boolean> {

	@Autowired
	private UserService userService;

	@Override
	protected Boolean doExecute(CoordinatesRequest request) {
		return userService.saveCoordinates(request);
	}
}
