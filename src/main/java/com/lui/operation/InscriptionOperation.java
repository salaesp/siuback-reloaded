package com.lui.operation;

import com.lui.guarani.model.Inscription;
import com.lui.request.AddInscriptionRequest;
import com.lui.service.ExamService;
import com.lui.service.SubjectService;
import com.lui.utils.InscriptionAction;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 04/06/15.
 */
@Component
public class InscriptionOperation extends AbstractLoginAwareOperation<AddInscriptionRequest, Inscription> {

	final static Logger LOG = Logger.getLogger(InscriptionOperation.class);


	@Autowired
	private ExamService examService;
	@Autowired
	private SubjectService subjectService;

	@Override
	protected Inscription doExecute(AddInscriptionRequest request) {
		if (InscriptionAction.SUBJECT_INSCRIPTION.equals(request.getAction())) {
			return subjectService.addInscription(request);
		}
		if (InscriptionAction.EXAM_INSCRIPTION.equals(request.getAction())) {
			return examService.addInscription(request);
		}
		return null;
	}
}
