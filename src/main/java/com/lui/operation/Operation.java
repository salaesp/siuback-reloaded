package com.lui.operation;

/**
 * Created by lui on 02/06/15.
 */
public interface Operation<I, O> {

	O execute(I request);

}
