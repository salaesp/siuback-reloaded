package com.lui.operation;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Sets;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.guarani.model.id.SubjectID;
import com.lui.request.UserCareerAwareRequest;
import com.lui.service.ExamService;
import com.lui.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by lui on 05/07/15.
 */
@Component
public class GetAvailableExamsOperation extends AbstractLoginAwareOperation<UserCareerAwareRequest, List<Subject>> {

	@Autowired
	private SubjectService subjectService;
	@Autowired
	private ExamService examService;

	@Override
	protected List<Subject> doExecute(UserCareerAwareRequest request) {
		final Set<SubjectID> subjectRegularities = FluentIterable.from(subjectService.listRegularities(request)).transform(new Function<SubjectRegularity, SubjectID>() {
			@Override
			public SubjectID apply(SubjectRegularity subjectRegularity) {
				return subjectRegularity.getSubject();
			}
		}).toSet();
		final Set<SubjectID> exams = Sets.newHashSet(FluentIterable.from(examService.list(request)).transform(new Function<Exam, SubjectID>() {
			@Override
			public SubjectID apply(Exam exam) {
				return exam.getSubject();
			}
		}));
		exams.addAll(FluentIterable.from(examService.listInscriptions(request)).transform(new Function<Inscription, SubjectID>() {
			@Override
			public SubjectID apply(Inscription inscription) {
				return inscription.getSubject();
			}
		}).toSet());

		return FluentIterable.from(subjectRegularities).filter(new Predicate<SubjectID>() {
			@Override
			public boolean apply(SubjectID subjectID) {
				return !exams.contains(subjectID);
			}
		}).transform(new Function<SubjectID, Subject>() {
			@Override
			public Subject apply(SubjectID subjectID) {
				return subjectService.getById(subjectID);
			}
		}).toList();
	}
}
