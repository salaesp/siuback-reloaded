package com.lui.operation;

import com.lui.guarani.model.CareerInscription;
import com.lui.model.UserBasicInfo;
import com.lui.request.SimpleLoginAwareRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 10/06/15.
 */
@Component
public class GetUserBasicInfoOperation extends AbstractLoginAwareOperation<SimpleLoginAwareRequest, UserBasicInfo> {

	@Autowired
	private Operation<SimpleLoginAwareRequest, List<CareerInscription>> getUserCareerInscriptionsOperation;

	@Override
	protected UserBasicInfo doExecute(SimpleLoginAwareRequest request) {
		UserBasicInfo info = new UserBasicInfo();
		info.setCareerInscriptions(getUserCareerInscriptionsOperation.execute(request));
		info.setLastSeen(request.getUserLoginInfo().getLastSeen());
		return info;
	}
}
