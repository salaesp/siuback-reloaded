package com.lui.operation;

import com.lui.exception.InvalidInscriptionException;
import com.lui.guarani.model.Inscription;
import com.lui.request.GetInscriptionRequest;
import com.lui.service.ExamService;
import com.lui.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 09/06/15.
 */
@Component
public class DeleteInscriptionOperation extends AbstractLoginAwareOperation<GetInscriptionRequest, Inscription> {

	@Autowired
	private ExamService examService;
	@Autowired
	private SubjectService subjectService;


	@Override
	protected Inscription doExecute(GetInscriptionRequest request) {
		if (examService.getInscription(request) != null) {
			return examService.deleteInscription(request);
		}
		if (subjectService.getInscription(request) != null) {
			return subjectService.deleteInscription(request);
		}
		throw new InvalidInscriptionException("La inscripción no existe");
	}
}
