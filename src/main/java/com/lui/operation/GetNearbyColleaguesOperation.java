package com.lui.operation;

import com.lui.model.NearbyInfo;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 11/06/15.
 */
@Component
public class GetNearbyColleaguesOperation extends AbstractLoginAwareOperation<SimpleLoginAwareRequest,List<NearbyInfo>> {
	@Autowired
	private UserService userService;

	@Override
	protected List<NearbyInfo> doExecute(SimpleLoginAwareRequest request) {
		return userService.getNearbyColleagues(request.getLoginKey());
	}
}
