package com.lui.operation;

import com.lui.guarani.model.AcademicHistory;
import com.lui.request.UserCareerAwareRequest;
import com.lui.service.AcademicHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by lui on 02/06/15.
 */
@Component
public class GetUserAcademicHistoryOperation extends AbstractLoginAwareOperation<UserCareerAwareRequest, AcademicHistory> {

	@Autowired
	private AcademicHistoryService academicHistoryService;

	@Override
	protected AcademicHistory doExecute(UserCareerAwareRequest request) {
		return academicHistoryService.getUserAcademicHistory(request);
	}
}
