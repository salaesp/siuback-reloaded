package com.lui.operation;

import com.lui.guarani.model.CareerInscription;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.service.CareerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
@Component
public class GetUserCareerInscriptionsOperation extends AbstractLoginAwareOperation<SimpleLoginAwareRequest, List<CareerInscription>> {

	@Autowired
	private CareerService careerService;


	@Override
	public List<CareerInscription> doExecute(SimpleLoginAwareRequest request) {
		return careerService.listInscriptions(request);
	}
}
