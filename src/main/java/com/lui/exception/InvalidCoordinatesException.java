package com.lui.exception;

/**
 * Created by lui on 10/06/15.
 */
public class InvalidCoordinatesException extends RuntimeException {
	public InvalidCoordinatesException(String message) {
		super(message);
	}
}
