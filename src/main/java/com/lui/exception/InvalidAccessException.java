package com.lui.exception;

/**
 * Created by lui on 31/05/15.
 */
public class InvalidAccessException extends RuntimeException {

	public InvalidAccessException(String message) {
		super(message);
	}
}
