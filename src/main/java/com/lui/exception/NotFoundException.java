package com.lui.exception;

/**
 * Created by lui on 31/05/15.
 */
public class NotFoundException extends RuntimeException {
	public NotFoundException(String message) {
		super(message);
	}
}
