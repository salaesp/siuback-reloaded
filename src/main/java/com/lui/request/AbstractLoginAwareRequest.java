package com.lui.request;

import com.lui.model.UserLoginInfo;

/**
 * Created by lui on 28/05/15.
 */
public abstract class AbstractLoginAwareRequest {

	private String loginKey;
	@Deprecated
	private String forcedBasicLoginAuth;
	private UserLoginInfo userLoginInfo;

	public AbstractLoginAwareRequest(String key) {
		this.loginKey = key;
	}

	public String getLoginKey() {
		return loginKey;
	}

	public void setLoginKey(String loginKey) {
		this.loginKey = loginKey;
	}

	public UserLoginInfo getUserLoginInfo() {
		return userLoginInfo;
	}

	public void setUserLoginInfo(UserLoginInfo userLoginInfo) {
		this.userLoginInfo = userLoginInfo;
	}

	@Deprecated
	public String getForcedBasicLoginAuth() {
		return forcedBasicLoginAuth;
	}

	@Deprecated
	public void setForcedBasicLoginAuth(String forcedBasicLoginAuth) {
		this.forcedBasicLoginAuth = forcedBasicLoginAuth;
	}
}
