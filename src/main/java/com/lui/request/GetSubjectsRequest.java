package com.lui.request;

import com.lui.guarani.model.id.CareerPlanID;

/**
 * Created by lui on 03/06/15.
 */
public class GetSubjectsRequest extends UserCareerAwareRequest {
	private boolean includeRegularities;
	private boolean includeInscriptions;

	public GetSubjectsRequest(String key, CareerPlanID careerPlanID, boolean includeRegularities, boolean includeInscriptions) {
		super(key, careerPlanID);
		this.includeRegularities = includeRegularities;
		this.includeInscriptions = includeInscriptions;
	}

	public boolean isIncludeRegularities() {
		return includeRegularities;
	}

	public void setIncludeRegularities(boolean includeRegularities) {
		this.includeRegularities = includeRegularities;
	}

	public boolean isIncludeInscriptions() {
		return includeInscriptions;
	}

	public void setIncludeInscriptions(boolean includeInscriptions) {
		this.includeInscriptions = includeInscriptions;
	}

}
