package com.lui.request;

import com.lui.guarani.model.id.SubjectID;
import com.lui.utils.InscriptionAction;

/**
 * Created by lui on 04/06/15.
 */
public class AddInscriptionRequest extends SimpleLoginAwareRequest {
	private SubjectID subjectId;
	private InscriptionAction action;
	private String variant;

	public AddInscriptionRequest(String key, SubjectID subjectId, InscriptionAction action, String variant) {
		super(key);
		this.subjectId = subjectId;
		this.action = action;
		this.variant = variant;
	}

	public InscriptionAction getAction() {
		return action;
	}

	public void setAction(InscriptionAction action) {
		this.action = action;
	}

	public SubjectID getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(SubjectID subjectId) {
		this.subjectId = subjectId;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}
}
