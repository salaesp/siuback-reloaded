package com.lui.utils;

/**
 * Created by lui on 14/05/15.
 */
public enum ResponseStatus {
	SUCCESSFUL,
	NOT_LOGGED_USER,
	INVALID_CREDENTIALS;
}
